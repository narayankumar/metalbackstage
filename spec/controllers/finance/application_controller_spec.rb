require 'rails_helper'

RSpec.describe Finance::ApplicationController, type: :controller do

let(:user) { FactoryGirl.create(:user) }
  
  before do
    allow(controller).to receive(:authenticate_user!)
    allow(controller).to receive(:current_user).and_return(user)
  end
  
  context "non-finance users" do
    it "are not able to access the index action" do
      get :index
      
      expect(response).to redirect_to "/"
      expect(flash[:alert]).to eq "You must be in finance to do that"
    end
  end  

end
