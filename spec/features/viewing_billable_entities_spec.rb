require "rails_helper"

RSpec.feature "Users can view billable entities" do
  let(:user) { FactoryGirl.create(:user, :admin) }
  
  before do
    unilever = FactoryGirl.create(:client, name: "Uni Lever")
    FactoryGirl.create(:billable_entity, client: unilever, name: "Sample Entity", phone: "022-9898278")
    
    colgate = FactoryGirl.create(:client, name: "Colgate")
    FactoryGirl.create(:billable_entity, client: colgate, name: "Another Sample Entity", phone: "Sample phone number")
    
    login_as(user)  
    visit "/"
  end
  
  scenario "for a given client" do
    click_link "Uni Lever"

    expect(page).to have_content "Sample Entity"
    expect(page).to_not have_content "Another Sample Entity"

    click_link "Sample Entity"

    expect(page).to have_content "Sample Entity"    
    expect(page).to have_content "022-9898278"
  end
end