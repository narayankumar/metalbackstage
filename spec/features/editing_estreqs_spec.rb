require "rails_helper"

RSpec.feature "Users can edit estimate requisitions" do
  let(:author) { FactoryGirl.create(:user, :admin) }
  let(:client) { FactoryGirl.create(:client) }
  
  before do
    login_as(author)
    job = FactoryGirl.create(:job, client: client, author: author)
    vendor = FactoryGirl.create(:vendor, name: "Example vendor")
    billa = FactoryGirl.create(:billable_entity, name: "Billa", client: client)
    estimate_requisition = FactoryGirl.create(:estimate_requisition, job: job, author: author, vendor: vendor)
    assign_role!(author, :manager, client)
    
    visit job_estimate_requisition_path(job, estimate_requisition)
    click_link "Edit Estimate requisition"
  end
  
  scenario "with valid attributes" do
    fill_in "Amount", with: "8250"
    click_button "Update Estimate requisition"
    
    expect(page).to have_content "Estimate requisition was updated"
    expect(page).to have_content "8250"
  end
  
  scenario "but not with invalid attributes" do
    fill_in "Details", with: ""
    click_button "Update Estimate requisition"
    
    expect(page).to have_content "Estimate requisition was not updated"
  end
end


