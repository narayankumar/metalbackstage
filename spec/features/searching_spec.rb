require "rails_helper"

RSpec.feature "Users can search for jobs matching criteria" do
  let(:user) { FactoryGirl.create(:user) }
  let(:client) { FactoryGirl.create(:client) }
  
  let!(:job_1) do
    FactoryGirl.create(:job, name: "Brand new job", client: client, author: user, tag_names: "iteration_1")
  end
  
  let!(:job_2) do
    FactoryGirl.create(:job, name: "Another brand new job", client: client, tag_names: "iteration_2")
  end
  
  before do
    assign_role!(user, :manager, client)
    login_as(user)
    visit client_path(client)
  end
  
  scenario "searching by tag" do
    fill_in "Search", with: "tag:iteration_1"
    click_button "Search"
    
    within("#jobs") do
      expect(page).to have_link "Brand new job"
      expect(page).to_not have_link "Another brand new job"
    end
  end
  
  scenario "when clicking on a tag" do
    click_link "Brand new job"
    click_link "iteration_1"
    
    within("#jobs") do
      expect(page).to have_content "Brand new job"
      expect(page).to_not have_content "Another brand new job"
    end
  end
end