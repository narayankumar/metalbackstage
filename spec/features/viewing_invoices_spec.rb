require "rails_helper"

RSpec.feature "Users can view invoices" do
  let(:author) { FactoryGirl.create(:user, :admin) }
  let(:client) { FactoryGirl.create(:client, name: "Sample client") }
  let(:billa) { FactoryGirl.create(:billable_entity, client: client) }
  
  before do
    login_as(author)
    job = FactoryGirl.create(:job, client: client, name: "Jobsample", author: author)
    estimate = FactoryGirl.create(:estimate, job: job, billable_entity: billa, name: "Sample Estimate", author: author)
    vendor = FactoryGirl.create(:vendor, name: "Example vendor")
    invoice = FactoryGirl.create(:invoice, estimate: estimate, author: author, title: "Example Invoice")
    assign_role!(author, :director, client)    
    
    visit "/"
  end
  
  scenario "for a given estimate" do
    click_link "Sample client"
    click_link "Jobsample"
    click_link "Sample Estimate"

    expect(page).to have_content "Example Invoice"
  end
end

