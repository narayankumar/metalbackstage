require "rails_helper"

RSpec.feature "Users can view estimate requisitions" do
  before do
  author = FactoryGirl.create(:user, :admin)
  client = FactoryGirl.create(:client, name: "Client")
  job = FactoryGirl.create(:job, client: client, name: "Jobsample", author: author)
    FactoryGirl.create(:estimate_requisition, job: job, title: "Sample title")
    login_as(author)
    assign_role!(author, :manager, client)
    visit "/"
  end
  
  scenario "for a given job" do
    click_link "Client"
    click_link "Jobsample"

    expect(page).to have_content "Sample title"
  end
end
