require "rails_helper"

RSpec.feature "Users can view retainer invoices" do
  before do
    author = FactoryGirl.create(:user, :admin)
    unilever = FactoryGirl.create(:client, name: "Uni Lever")
    billa = FactoryGirl.create(:billable_entity, name: "Billa", client: unilever)
    FactoryGirl.create(:retainer_invoice, client: unilever, title: "Sample Retainer invoice", narration: "Sample narration", author: author, billable_entity: billa)
    
    colgate = FactoryGirl.create(:client, name: "Colgate")
    billa = FactoryGirl.create(:billable_entity, name: "Billa", client: unilever)
    FactoryGirl.create(:retainer_invoice, client: colgate, title: "Another Sample retainer", narration: "Another sample narration", author: author)
    
    login_as(author)
    visit "/"
  end
  
  scenario "for a given client" do
    click_link "Uni Lever"

    expect(page).to have_content "Sample Retainer invoice"
    expect(page).to_not have_content "Another Sample retainer"

    click_link "Sample Retainer invoice"
    
    expect(page).to have_content "Sample Retainer invoice"
    expect(page).to have_content "Sample narration"
  end
end