require "rails_helper"

RSpec.feature "Users can delete billable entities" do
  let(:user) { FactoryGirl.create(:user, :admin) }
  let(:client) { FactoryGirl.create(:client) }
  let(:billable_entity) { FactoryGirl.create(:billable_entity, client: client) }
  before do
    login_as(user)
    visit client_billable_entity_path(client, billable_entity)
  end
  
  scenario "successfully" do
    click_link "Delete Billable entity"
    
    expect(page).to have_content "Billable entity was deleted"
    expect(page.current_url).to eq client_url(client)
  end
end