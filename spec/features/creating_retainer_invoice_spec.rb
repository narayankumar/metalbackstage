require "rails_helper"

RSpec.feature "Users can create new retainer invoice for a client" do
  let(:author) { FactoryGirl.create(:user, :admin) }
 
  before do
    client = FactoryGirl.create(:client, name: "Unilever")
    billable_entity = FactoryGirl.create(:billable_entity, client: client)
    
    login_as(author)
    visit client_path(client)
    click_link "New Retainer invoice"
  end
  
  scenario "with valid attributes" do
    fill_in "Title", with: "Monthly retainer"   
    click_button "Create Retainer invoice"

    expect(page).to have_content "Retainer invoice has been created"
    expect(page).to have_content "Author: #{author.email}"
  end
  
  scenario "but not with invalid attributes" do
    click_button "Create Retainer invoice"
    
    expect(page).to have_content "Retainer invoice was not created"
    expect(page).to have_content "Title can't be blank"
  end
end
