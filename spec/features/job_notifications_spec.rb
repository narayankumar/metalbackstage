require "rails_helper"

RSpec.feature "Users can receive notifications about job updates" do
  let(:alice) { FactoryGirl.create(:user, email: "alice@example.com") }
  let(:bob) { FactoryGirl.create(:user, email: "bob@example.com") }
  let(:client) { FactoryGirl.create(:client) }
  let(:job) do
    FactoryGirl.create(:job, client: client, author: alice)    
  end
  
  before do
    assign_role!(alice, :manager, client)
    assign_role!(bob, :manager, client)
    
    login_as(bob)
    visit client_job_path(client, job)
  end
  
  scenario "job authors automatically receive notifications" do
    fill_in "Text", with: "Is it out yet?"
    click_button "Create Comment"
    
    email = find_email!(alice.email)
    expected_subject = "[backstage] #{client.name} - #{job.name}"
    expect(email.subject).to eq expected_subject
    
    click_first_link_in_email(email)
    expect(current_path).to eq client_job_path(client,job)
  end
  
  scenario "comment authors are automatically subscribed to a job" do
    fill_in "Text", with: "Is it out yet?"
    click_button "Create Comment"
    click_link "Sign out"
    
    reset_mailer
    
    login_as(alice)
    
    visit client_job_path(client, job)
    fill_in "Text", with: "Not yet - sorry!"
    click_button "Create Comment"
    
    expect(page).to have_content "Comment has been created"
    expect(unread_emails_for(bob.email).count).to eq 1
    expect(unread_emails_for(alice.email).count).to eq 0
  end
end