require "rails_helper"

RSpec.feature "Users can view unbillable purchase orders" do
  let(:author) { FactoryGirl.create(:user) }
  let(:vendor) { FactoryGirl.create(:vendor) }
  
  before do
    unilever = FactoryGirl.create(:client, name: "Uni Lever")
    assign_role!(author, :handler, unilever)
    FactoryGirl.create(:unbillable_purchase_order, client: unilever, details: "Lots of details for unbillable_purchase_order", amount: "25000", author: author, vendor: vendor )
    
    colgate = FactoryGirl.create(:client, name: "Colgate")
    FactoryGirl.create(:unbillable_purchase_order, client: colgate, details: "Another Sample unbillable_purchase_order", amount: "30000", author: author)
  end
  
  scenario "for a given client" do
    login_as(author)
    
    visit "/"
    click_link "Uni Lever"

    expect(page).to have_content "Lots of details for unbillable_purchase_order"
    expect(page).to_not have_content "Another Sample unbillable_purchase_order"
  end
end