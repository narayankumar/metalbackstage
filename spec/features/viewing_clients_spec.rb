require "rails_helper"

RSpec.feature "Users can view clients" do
  let(:user) { FactoryGirl.create(:user) }
  let(:client) { FactoryGirl.create(:client, name: "Uni Lever") }
  
  before do
    login_as(user)
    assign_role!(user, :member, client)
  end
  
  scenario "with client details" do  
    visit "/"
    click_link "Uni Lever"
    
    expect(page.current_url).to eq client_url(client)
  end
  
  scenario "unless they don't have permission" do
    FactoryGirl.create(:client, name: "Hidden")
    visit "/"  
    expect(page).not_to have_content "Hidden"
  end
end