require "rails_helper"

RSpec.feature "Users can delete jobs" do
  let(:author) { FactoryGirl.create(:user) }
  let(:client) { FactoryGirl.create(:client) }
  
  let(:job) do
    FactoryGirl.create(:job, client: client, author: author)
  end
  
  before do
    login_as(author)
    assign_role!(author, :manager, client)
    visit client_job_path(client, job)
  end
  
  scenario "successfully" do
    click_link "Delete Job"
    
    expect(page).to have_content "Job was deleted"
    expect(page.current_url).to eq client_url(client)
  end
end