require "rails_helper"

RSpec.feature "Users can edit invoice requisitions" do
  let(:author) { FactoryGirl.create(:user, :admin) }
  let(:client) { FactoryGirl.create(:client) }
  let(:billa) { FactoryGirl.create(:billable_entity, client: client) }
  
  before do
    login_as(author)
    job = FactoryGirl.create(:job, client: client)
    estimate = FactoryGirl.create(:estimate, job: job, billable_entity: billa)
    vendor = FactoryGirl.create(:vendor, name: "Example vendor")
    invoice_requisition = FactoryGirl.create(:invoice_requisition, estimate: estimate, author: author)
    assign_role!(author, :finance, client)
    
    visit estimate_invoice_requisition_path(estimate, invoice_requisition)
    click_link "Edit Invoice requisition"
  end
  
  scenario "with valid attributes" do
    fill_in "Notes", with: "Revised notes for requisition"
    click_button "Update Invoice requisition"
    
    expect(page).to have_content "Invoice requisition was updated"
    expect(page).to have_content "Revised notes for requisition"  
  end
  
  scenario "but not with invalid attributes" do
    fill_in "Notes", with: ""
    click_button "Update Invoice requisition"
    
    expect(page).to have_content "Invoice requisition was not updated"
  end
end
    