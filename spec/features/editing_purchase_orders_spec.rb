require "rails_helper"

RSpec.feature "Users can edit purchase_orders" do
  let(:author) { FactoryGirl.create(:user, :admin) }
  let(:client) { FactoryGirl.create(:client) }
  let(:billa) { FactoryGirl.create(:billable_entity, client: client) }
  
  before do
    login_as(author)
    job = FactoryGirl.create(:job, client: client)
    estimate = FactoryGirl.create(:estimate, job: job, billable_entity: billa)
    vendor = FactoryGirl.create(:vendor, name: "Example vendor")
    purchase_order = FactoryGirl.create(:purchase_order, estimate: estimate, author: author, vendor: vendor)
    assign_role!(author, :handler, client)
    
    visit estimate_purchase_order_path(estimate, purchase_order)
    click_link "Edit PO"
  end
  
  scenario "with valid attributes" do
    fill_in "Title", with: "Revised purchase order"
    click_button "Update Purchase order"
    
    expect(page).to have_content "Purchase order was updated"
    expect(page).to have_content "Revised purchase order"
  end
  
  scenario "but not with invalid attributes" do
    fill_in "Title", with: ""
    click_button "Update Purchase order"
    
    expect(page).to have_content "Purchase order was not updated"
  end
end
