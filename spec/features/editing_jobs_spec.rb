require "rails_helper"

RSpec.feature "Users can edit job" do
  let(:author) { FactoryGirl.create(:user, :admin) }
  let(:client) { FactoryGirl.create(:client) }
  
  let(:job) do
    FactoryGirl.create(:job, client: client, author: author)
  end
  
  before do
    assign_role!(author, :manager, client)
    login_as(author)
    visit client_job_path(client, job)
    click_link "Edit Job"
  end
  
  scenario "with valid attributes" do
    fill_in "Name", with: "Revised"
    click_button "Update Job"
    
    expect(page).to have_content "Job was updated" 
    expect(page).to have_content "Revised"
    expect(page).not_to have_content job.name
  end
  
  scenario "but no invalid entries" do
    fill_in "Name", with: ""
    click_button "Update Job"
    
    expect(page).to have_content "Job was not updated"
    expect(page).to have_content "Name can't be blank"
  end
end