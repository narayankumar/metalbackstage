require "rails_helper"

RSpec.feature "Users can edit retainer invoice" do
  let(:author) { FactoryGirl.create(:user, :admin) }
  let(:client) { FactoryGirl.create(:client) }
  let(:billable_entity) { FactoryGirl.create(:billable_entity, client: client) }
  
  
  before do
    login_as(author)
    retainer_invoice = FactoryGirl.create(:retainer_invoice, client: client, billable_entity: billable_entity, author: author)
    
    visit client_retainer_invoice_path(client, retainer_invoice)
    click_link "Edit Invoice"
  end
  
  scenario "with valid attributes" do
    fill_in "Title", with: "Revised"
    
    click_button "Update Retainer invoice"
    
    expect(page).to have_content "Retainer invoice was updated"
    
    within("#retainer_invoices") do
      expect(page).to have_content "Revised"
    end
  end
  
  scenario "but no invalid entries" do
    fill_in "Title", with: ""
    click_button "Update Retainer invoice"
    
    expect(page).to have_content "Retainer invoice was not updated"
    expect(page).to have_content "Title can't be blank"
  end
end