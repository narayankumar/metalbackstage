require "rails_helper"

RSpec.feature "Users can create estimate reqs" do
  let(:author) { FactoryGirl.create(:user) }
  let(:client) { FactoryGirl.create(:client) }
 
  before do
    login_as(author)
    job = FactoryGirl.create(:job, client: client, author: author)
    vendor = FactoryGirl.create(:vendor, name: "Example vendor")
    billable_entity = FactoryGirl.create(:billable_entity, client: client)
    assign_role!(author, :manager, client)
    
    visit client_job_path(client, job)
    click_link "New Estimate Requisition"
  end
  
  scenario "with valid attributes" do
    fill_in "Details", with: "Sample Estimate details"
    fill_in "Amount", with: "12000"
    click_button "Create Estimate requisition"

    expect(page).to have_content "Sample Estimate details"
  end
  
  scenario "unless using invalid attributes" do
    click_button "Create Estimate requisition"
    expect(page).to have_content "Estimate Requisition was not created"
    expect(page).to have_content "Details can't be blank"
  end
end
