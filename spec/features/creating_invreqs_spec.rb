require "rails_helper"

RSpec.feature "Users can create invoice requisitions" do
  let(:author) { FactoryGirl.create(:user, :admin) }
  let(:client) { FactoryGirl.create(:client) }
  
  before do
   login_as(author)
    job = FactoryGirl.create(:job, client: client, author: author)
    vendor = FactoryGirl.create(:vendor, name: "Example vendor")
    billable_entity = FactoryGirl.create(:billable_entity, client: client)
    estimate = FactoryGirl.create(:estimate, job: job, author: author, billable_entity: billable_entity)
    assign_role!(author, :handler, client)
    
    visit client_job_estimate_path(client, job, estimate)
    click_link "New Invoice requisition"
  end
  
  scenario "with valid attributes" do
    fill_in "Notes", with: "Sample invoice requisition details"
    click_button "Create Invoice requisition"

    expect(page).to have_content "Sample invoice requisition details"
  end
  
  scenario "unless using invalid attributes" do
    click_button "Create Invoice requisition"
    expect(page).to have_content "Invoice Requisition was not created"
    expect(page).to have_content "Notes can't be blank"
  end
end
  