require "rails_helper"

RSpec.feature "Users can delete unbillable purchase orders" do
  let(:author) { FactoryGirl.create(:user, :admin)}
  let(:client) { FactoryGirl.create(:client) }
  let(:vendor) { FactoryGirl.create(:vendor) }
  
  before do
    login_as(author)
    unbillable_purchase_order = FactoryGirl.create(:unbillable_purchase_order, client: client, author: author, vendor: vendor)
    
    visit client_unbillable_purchase_order_path(client, unbillable_purchase_order)
  end
  
  scenario "successfully" do
    click_link "Delete NBPO"
    
    expect(page).to have_content "Unbillable purchase order was deleted"
    expect(page.current_url).to eq client_url(client)
  end
end
