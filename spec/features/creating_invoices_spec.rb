require "rails_helper"

RSpec.feature "Users can create invoices" do
  let(:author) { FactoryGirl.create(:user, :admin) }
  let(:client) { FactoryGirl.create(:client) }

  before do
    login_as(author)
    job = FactoryGirl.create(:job, client: client, author: author)
    vendor = FactoryGirl.create(:vendor, name: "Example vendor")
    billable_entity = FactoryGirl.create(:billable_entity, client: client)
    estimate = FactoryGirl.create(:estimate, job: job, author: author, billable_entity: billable_entity)
    assign_role!(author, :director, client)
    
    visit client_job_estimate_path(client, job, estimate)
    click_link "New Invoice"
  end
  
  scenario "with valid attributes" do
    fill_in "Title", with: "Sample Invoice"
    fill_in "Details", with: "Sample invoice details"
    click_button "Create Invoice"

    expect(page).to have_content "Invoice has been created"
    expect(page).to have_content "Author: #{author.email}"
  end
  
  scenario "unless using invalid attributes" do
    click_button "Create Invoice"
    expect(page).to have_content "Invoice was not created"
    expect(page).to have_content "Title can't be blank"
    expect(page).to have_content "Details can't be blank"
  end
end
