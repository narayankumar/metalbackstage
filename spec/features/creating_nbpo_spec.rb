require "rails_helper"

RSpec.feature "Users can create new unbillable purchase order for a client" do
  let(:author) { FactoryGirl.create(:user) }
  
  before do
    login_as(author)
    client = FactoryGirl.create(:client, name: "Unilever")
    vendor = FactoryGirl.create(:vendor, name: "Example vendor")
    assign_role!(author, :handler, client)
    
    visit client_path(client)
    click_link "New NBPO"
  end
  
  scenario "with valid attributes" do
    fill_in "Details", with: "This and that details"
    fill_in "Amount", with: "98210"
    select "Example vendor", from: "Vendor"
    click_button "Create Unbillable purchase order"

    expect(page).to have_content "Example vendor"
  end
  
  scenario "but not with invalid attributes" do
    click_button "Create Unbillable purchase order"
    
    expect(page).to have_content "Unbillable purchase order was not created"
    expect(page).to have_content "Details can't be blank"
    expect(page).to have_content "Amount can't be blank"
  end
end
