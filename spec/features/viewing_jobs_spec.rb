require "rails_helper"

RSpec.feature "Users can view jobs" do
  before do
    author = FactoryGirl.create(:user)
    unilever = FactoryGirl.create(:client, name: "Uni Lever")
    FactoryGirl.create(:job, client: unilever, name: "Sample Job", description: "Sample text for Sample Job", author: author)
    assign_role!(author, :member, unilever)
    
    colgate = FactoryGirl.create(:client, name: "Colgate")
    assign_role!(author, :member, colgate)
    FactoryGirl.create(:job, client: colgate, author: author, name: "Another Sample Job", description: "Sample text for Another Sample Job")
    login_as(author)
    visit "/"
  end
  
  scenario "for a given client" do
    click_link "Uni Lever"

    expect(page).to have_content "Sample Job"
    expect(page).to_not have_content "Another Sample Job"

    click_link "Sample Job"
    
    expect(page).to have_content "Sample Job"    
    expect(page).to have_content "Sample text for Sample Job"
  end
end