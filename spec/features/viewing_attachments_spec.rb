require "rails_helper"

RSpec.feature "Users can view a job's attached files" do
  let(:user) { FactoryGirl.create :user }
  let(:client) { FactoryGirl.create :client }
  let(:job) { FactoryGirl.create :job, client: client, author: user }
  let!(:attachment) { FactoryGirl.create :attachment, job: job, file_to_attach: "spec/fixtures/speed.txt" }
  
  before do
    assign_role!(user, :member, client)
    login_as(user)
  end
  
  scenario "successfully" do
    visit client_job_path(client, job)
    click_link "speed.txt"
    
    expect(current_path).to eq attachment_path(attachment)
    expect(page).to have_content "The blink tag can blink faster"
  end
end
