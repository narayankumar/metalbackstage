require "rails_helper"

RSpec.feature "Users can create new billable entity for a client" do
  let(:user) { FactoryGirl.create(:user, :admin) }
  
  before do
    login_as(user)
    client = FactoryGirl.create(:client, name: "Unilever")
    visit client_path(client)
    click_link "New Billable entity"
  end
  
  scenario "with valid attributes" do
    fill_in "Name", with: "ABC and co"
    fill_in "Phone", with: "98210-99098"
    click_button "Create Billable entity"

    expect(page).to have_content "Billable entity has been created"
  end
  
  scenario "but not with invalid attributes" do
    click_button "Create Billable entity"
    
    expect(page).to have_content "Billable entity was not created"
    expect(page).to have_content "Name can't be blank"
  end
end