require "rails_helper"

RSpec.feature "Users can comment on jobs" do
  let(:user) { FactoryGirl.create(:user) }
  let(:client) { FactoryGirl.create(:client) }
  let(:job) { FactoryGirl.create(:job, client: client, author: user) }
  
  before do
    login_as(user)
    assign_role!(user, :director, client)
  end
  
  scenario "with valid attributes" do
    visit client_job_path(client, job)
    fill_in "Text", with: "Added a comment!"
    click_button "Create Comment"
    
    expect(page).to have_content "Comment has been created"
    within("#comments") do
      expect(page).to have_content "Added a comment!"
    end
  end
  
  scenario "but not with invalid attributes" do
    visit client_job_path(client, job)
    click_button "Create Comment"
    
    expect(page).to have_content "Comment was not created"
  end
  
  scenario "when changing a job's status" do
    FactoryGirl.create(:status, name: "Production")
    
    visit client_job_path(client, job)
    fill_in "Text", with: "This is a real issue"
    select "Production", from: "Status"
    click_button "Create Comment"
    
    expect(page).to have_content "Comment has been created"
    within(".job-status .status") do
      expect(page).to have_content "Production"
    end
  end
  
  scenario "when adding a new tag to job" do
    visit client_job_path(client, job)
    expect(page).not_to have_content "bug"
    
    fill_in "Text", with: "Adding the bug tag"
    fill_in "Tags", with: "bug"
    click_button "Create Comment"
    
    expect(page).to have_content "Comment has been created"
    within("#tags") do
      expect(page).to have_content "bug"
    end
  end
end