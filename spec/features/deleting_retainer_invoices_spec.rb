require "rails_helper"

RSpec.feature "Users can delete retainer invoices" do
  let(:author) { FactoryGirl.create(:user, :admin) }
  let(:client) { FactoryGirl.create(:client) }
  let(:billable_entity) { FactoryGirl.create(:billable_entity, client: client) }
  
  before do
    login_as(author)
    retainer_invoice = FactoryGirl.create(:retainer_invoice, client: client, billable_entity: billable_entity, author: author)
    visit client_retainer_invoice_path(client, retainer_invoice)
  end
  
  scenario "successfully" do
    click_link "Delete Invoice"
    
    expect(page).to have_content "Retainer invoice was deleted"
    expect(page.current_url).to eq client_url(client)
  end
end