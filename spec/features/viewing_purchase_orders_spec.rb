require "rails_helper"

RSpec.feature "Users can view purchase orders" do
  before do
  author = FactoryGirl.create(:user, :admin)
  client = FactoryGirl.create(:client, name: "Client")
  billa = FactoryGirl.create(:billable_entity, client: client)
  job = FactoryGirl.create(:job, client: client, name: "Jobsample", author: author)
  estimate = FactoryGirl.create(:estimate, job: job, name: "Sample Estimate", author: author, billable_entity: billa)
  FactoryGirl.create(:purchase_order, estimate: estimate, title: "Example purchase order", author: author)
  
  assign_role!(author, :handler, client)
  login_as(author)
  visit "/"
  end
  
  scenario "for a given estimate" do
    click_link "Client"
    click_link "Jobsample"
    click_link "Sample Estimate"

    expect(page).to have_content "Example purchase order"
  end
end