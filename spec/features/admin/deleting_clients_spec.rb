require "rails_helper"

RSpec.feature "Users can delete clients" do
  before do
    login_as(FactoryGirl.create(:user, :admin))  
  end
  
  scenario "successfully" do
    client = FactoryGirl.create(:client, name: "Uni Lever")
    visit "/"
    click_link "Uni Lever"
    click_link "Delete Client"

    expect(page).to have_content "Client was deleted"
    expect(page.current_url).to eq clients_url
    expect(page).to have_no_content "Uni Lever"
  end
end