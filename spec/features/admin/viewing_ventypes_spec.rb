require "rails_helper"

RSpec.feature "Users can view ventypes" do
  before do
    login_as(FactoryGirl.create(:user, :admin))
  end
  
  scenario "with ventype details" do
    ventype = FactoryGirl.create(:ventype, name: "Ventype")
    visit admin_ventypes_path
  
    click_link "Ventype"
    
    expect(page.current_url).to eq admin_ventype_url(ventype)
  end
end