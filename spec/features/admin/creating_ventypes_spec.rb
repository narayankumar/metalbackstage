require "rails_helper"

RSpec.feature "Users can create ventypes" do
  before do
    login_as(FactoryGirl.create(:user, :admin))
    visit admin_ventypes_path
    click_link "New Ventype"
  end
  
  scenario "with valid attributes" do    
    fill_in "Name", with: "New Ventype"
    click_button "Create Ventype"
    
    expect(page).to have_content "Ventype has been created"
    
    ventype = Ventype.find_by(name: "New Ventype")
    expect(page.current_url).to eq admin_ventype_url(ventype)

  end
  
  scenario "but not with invalid attributes" do
    click_button "Create Ventype"
    
    expect(page).to have_content "Ventype was not created"
    expect(page).to have_content "Name can't be blank"
  end
end