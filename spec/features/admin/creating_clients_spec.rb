require "rails_helper"

RSpec.feature "Users can create clients" do
  before do
    login_as(FactoryGirl.create(:user, :admin))
    visit "/"
    click_link "New Client"
  end
  
  scenario "with valid attributes" do    
    fill_in "Name", with: "Uni Lever"
    fill_in "Description", with: "The biggie account for everyone"
    click_button "Create Client"
    
    expect(page).to have_content "Client has been created"
    
    client = Client.find_by(name: "Uni Lever")
    expect(page.current_url).to eq client_url(client)
    
    title = "Uni Lever | Clients | Metal Backstage"
    expect(page).to have_title title
  end
  
  scenario "but not with invalid attributes" do
    click_button "Create Client"
    
    expect(page).to have_content "Client was not created"
    expect(page).to have_content "Name can't be blank"
  end
end