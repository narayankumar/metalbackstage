require "rails_helper"

RSpec.feature "Users can create new vendor for a ventype" do
  
  before do
    login_as(FactoryGirl.create(:user, :admin))

    ventype = FactoryGirl.create(:ventype, name: "Photography")
    visit admin_ventype_path(ventype)
    click_link "New Vendor"
  end
  
  scenario "with valid attributes" do
    fill_in "Name", with: "ABC and co"
    click_button "Create Vendor"

    expect(page).to have_content "Vendor has been created"
  end
  
  scenario "but not with invalid attributes" do
    click_button "Create Vendor"
    
    expect(page).to have_content "Vendor was not created"
    expect(page).to have_content "Name can't be blank"
  end
end