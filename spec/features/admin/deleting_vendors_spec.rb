require "rails_helper"

RSpec.feature "Users can delete vendors" do
  before do
    login_as(FactoryGirl.create(:user, :admin))
  end
  
  scenario "successfully" do
    ventype = FactoryGirl.create(:ventype, name: "Photography")
    vendor = FactoryGirl.create(:vendor, ventype: ventype, name: "Fotoman")
    visit admin_ventype_vendor_path(ventype, vendor)
    
    click_link "Delete Vendor"
    
    expect(page).to have_content "Vendor was deleted"
    expect(page.current_url).to eq admin_ventype_url(ventype)
  end
end