require "rails_helper"

RSpec.feature "Users can edit ventypes" do
  
  before do
    login_as(FactoryGirl.create(:user, :admin))
    ventype = FactoryGirl.create(:ventype, name: "Uni Lever")
    visit admin_ventypes_path
    click_link "Uni Lever"
    click_link "Edit Ventype"
  end
  
  scenario "successfully" do
    fill_in "Name", with: "Unilever Revised"
    click_button "Update Ventype"

    expect(page).to have_content "Ventype was updated"
    expect(page).to have_content "Unilever Revised"
  end
  
  scenario "but not without valid entries" do
    fill_in "Name", with: ""
    click_button "Update Ventype"
    
    expect(page).to have_content "Ventype was not updated"
  end
  
end