require "rails_helper"

RSpec.feature "Users can edit vendors" do
  
  before do
    login_as(FactoryGirl.create(:user, :admin))
  
    ventype = FactoryGirl.create(:ventype, name: "Photography")
    vendor = FactoryGirl.create(:vendor, ventype: ventype, name: "Fotoman")
    
    visit admin_ventype_vendor_path(ventype, vendor)
    click_link "Edit Vendor"
  end
  
  scenario "with valid attributes" do
    fill_in "Name", with: "Revised"
    click_button "Update Vendor"
    
    expect(page).to have_content "Vendor was updated"
    
    within("#vendors h4") do
      expect(page).to have_content "Revised"
      expect(page).not_to have_content "Fotoman"
    end
  end
  
  scenario "but no invalid entries" do
    fill_in "Name", with: ""
    click_button "Update Vendor"
    
    expect(page).to have_content "Vendor was not updated"
    expect(page).to have_content "Name can't be blank"
  end
end
    