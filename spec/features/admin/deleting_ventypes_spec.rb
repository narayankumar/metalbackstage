require "rails_helper"

RSpec.feature "Users can delete ventypes" do
  before do
  login_as(FactoryGirl.create(:user, :admin))
  ventype = FactoryGirl.create(:ventype, name: "New Vendor")
  end
  
  scenario "successfully" do  
    visit admin_ventypes_path
    click_link "New Vendor"
    click_link "Delete Ventype"

    expect(page).to have_content "Ventype was deleted"
    expect(page.current_url).to eq admin_ventypes_url
    expect(page).to have_no_content "New Vendor"
  end
end