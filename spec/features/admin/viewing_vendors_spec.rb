require "rails_helper"

RSpec.feature "Users can view vendors" do
  before do
    login_as(FactoryGirl.create(:user, :admin))
    
    photo = FactoryGirl.create(:ventype, name: "Photography")
    FactoryGirl.create(:vendor, ventype: photo, name: "Sample Vendor")
    
    digital = FactoryGirl.create(:ventype, name: "Digitalia")
    FactoryGirl.create(:vendor, ventype: digital, name: "Another Sample Vendor")
    visit admin_ventypes_path
  end
  
  scenario "for a given ventype" do
    click_link "Photography"

    expect(page).to have_content "Sample Vendor"
    expect(page).to_not have_content "Another Sample Vendor"

    click_link "Sample Vendor"
    
    within("#vendors h4") do
      expect(page).to have_content "Sample Vendor"
    end
  end
end