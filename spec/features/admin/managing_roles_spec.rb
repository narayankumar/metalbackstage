require "rails_helper"

RSpec.feature "Admins can manage user's roles" do
  let(:admin) { FactoryGirl.create(:user, :admin) }
  let(:user) { FactoryGirl.create(:user) }
  
  let!(:unilever) { FactoryGirl.create(:client, name: "Uni Lever") }
  let!(:nirma) { FactoryGirl.create(:client, name: "Nirma") }
  
  before do
    login_as(admin)
  end
  
  scenario "when assigning roles to existing user" do
    visit admin_user_path(user)
    click_link "Edit Metalloid"
    
    select "Member", from: "Uni Lever"
    select "Manager", from: "Nirma"
    
    click_button "Update User"
    
    expect(page).to have_content "Metalloid has been updated"
    
    click_link user.email
    
    expect(page).to have_content "Uni Lever: Member"
    expect(page).to have_content "Nirma: Manager"
  end
  
  scenario "when assigning roles to new user" do
    visit new_admin_user_path
    
    fill_in "Email", with: "newuser@metalcomm.com"
    fill_in "Password", with: "password"
    
    select "Handler", from: "Uni Lever"
    click_button "Create User"
    click_link "newuser@metalcomm.com"
    
    expect(page).to have_content "Uni Lever: Handler"
    expect(page).not_to have_content "Nirma"
  end
end