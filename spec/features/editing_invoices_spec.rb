require "rails_helper"

RSpec.feature "Users can edit invoices" do
  let(:author) { FactoryGirl.create(:user, :admin) }
  let(:client) { FactoryGirl.create(:client) }
  let(:billa) { FactoryGirl.create(:billable_entity, client: client) }
  
  before do
    login_as(author)
    job = FactoryGirl.create(:job, client: client)
    estimate = FactoryGirl.create(:estimate, job: job, billable_entity: billa)
    vendor = FactoryGirl.create(:vendor, name: "Example vendor")
    invoice = FactoryGirl.create(:invoice, estimate: estimate, author: author)
    assign_role!(author, :director, client)
    
    visit estimate_invoice_path(estimate, invoice)
    click_link "Edit Invoice"
  end
  
  scenario "with valid attributes" do
    fill_in "Title", with: "Revised Invoice"
    click_button "Update Invoice"
    
    expect(page).to have_content "Invoice was updated"
    expect(page).to have_content "Revised Invoice"
  end
  
  scenario "but not with invalid attributes" do
    fill_in "Title", with: ""
    click_button "Update Invoice"
    
    expect(page).to have_content "Invoice was not updated"
  end
end

