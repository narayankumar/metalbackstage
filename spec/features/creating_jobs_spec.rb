require "rails_helper"

RSpec.feature "Users can create new jobs for a client" do
  let(:user) { FactoryGirl.create(:user) }
  
  before do
    login_as(user)
    client = FactoryGirl.create(:client, name: "Unilever")
    assign_role!(user, :manager, client)
    
    visit client_path(client)
    click_link "New Job"
  end
  
  scenario "with valid attributes" do
    fill_in "Name", with: "Sales brochure"
    fill_in "Description", with: "The brochure that should be designed to sell and sell."
    click_button "Create Job"

    expect(page).to have_content "Job has been created"
    expect(page).to have_content "Author: #{user.email}"
  end
  
  scenario "but not with invalid attributes" do
    click_button "Create Job"
    
    expect(page).to have_content "Job was not created"
    expect(page).to have_content "Name can't be blank"
    expect(page).to have_content "Description can't be blank"
  end
  
  scenario "but not with description too short" do
    fill_in "Name", with: "Sales brochure"
    fill_in "Description", with: "The "
    click_button "Create Job"
    
    expect(page).to have_content "Job was not created"
    expect(page).to have_content "Description is too short"    
  end
  
  scenario "with multiple attachments", js: true do
    fill_in "Name", with: "Add documentation"
    fill_in "Description", with: "The blink tag has a speed attribute"
    
    attach_file "File #1", Rails.root.join("spec/fixtures/speed.txt")
    click_link "Add another file"
    
    attach_file "File #2", Rails.root.join("spec/fixtures/spin.txt")    
    click_button "Create Job"
    
    expect(page).to have_content "Job has been created"
    
    within(".attachments") do
      expect(page).to have_content "speed.txt"
      expect(page).to have_content "spin.txt"
    end
  end
  
  scenario "persisting file uploads across form displays" do
    attach_file "File #1", "spec/fixtures/speed.txt"
    click_button "Create Job"
    
    fill_in "Name", with: "Add documentation"
    fill_in "Description", with: "The blink tag has a speed attribute"
    click_button "Create Job"
    
    within(".attachments") do
      expect(page).to have_content "speed.txt"
    end
  end
  
  scenario "with associated tags" do
    fill_in "Name", with: "Add documentation"
    fill_in "Description", with: "The blink tag has a speed attribute"
    fill_in "Tags", with: "browser campaign"
    click_button "Create Job"
    
    expect(page).to have_content "Job has been created"
    within("#tags") do
      expect(page).to have_content "browser"
      expect(page).to have_content "campaign"
    end
  end
end



