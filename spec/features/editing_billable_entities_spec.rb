require "rails_helper"

RSpec.feature "Users can edit billable entity" do
  let(:user) { FactoryGirl.create(:user, :admin)}
  let(:client) { FactoryGirl.create(:client) }
  let(:billable_entity) { FactoryGirl.create(:billable_entity, client: client) }
  
  before do
    login_as(user)
    visit client_billable_entity_path(client, billable_entity)
    click_link "Edit Billable entity"
  end
  
  scenario "with valid attributes" do
    fill_in "Name", with: "Revised"
    click_button "Update Billable entity"
    
    expect(page).to have_content "Billable entity was updated"
    
    within("#billable_entities") do
      expect(page).to have_content "Revised"
      expect(page).not_to have_content billable_entity.name
    end
  end
  
  scenario "but no invalid entries" do
    fill_in "Name", with: ""
    click_button "Update Billable entity"
    
    expect(page).to have_content "Billable entity was not updated"
    expect(page).to have_content "Name can't be blank"
  end
end