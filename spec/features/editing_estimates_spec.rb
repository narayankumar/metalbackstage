require "rails_helper"

RSpec.feature "Users can edit estimates" do
  let(:author) { FactoryGirl.create(:user, :admin) }
  let(:client) { FactoryGirl.create(:client) }
  
  before do
    login_as(author)
    billa = FactoryGirl.create(:billable_entity, client: client)
    job = FactoryGirl.create(:job, client: client, author: author)
    estimate = FactoryGirl.create(:estimate, job: job, author: author, billable_entity: billa)
    
    visit client_job_estimate_path(client, job, estimate)
    click_link "Edit Estimate"
  end
  
  scenario "with valid attributes" do
    fill_in "Name", with: "Revised Estimate"
    click_button "Update Estimate"
    
    expect(page).to have_content "Estimate was updated"
    expect(page).to have_content "Revised Estimate"
  end
  
  scenario "but not with invalid attributes" do
    fill_in "Name", with: ""
    click_button "Update Estimate"
    
    expect(page).to have_content "Estimate was not updated"
  end
end
