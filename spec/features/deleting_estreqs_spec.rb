require "rails_helper"

RSpec.feature "Users can delete estimate requisitions" do
  let(:author) { FactoryGirl.create(:user, :admin) }
  let(:client) { FactoryGirl.create(:client) }
  let(:vendor) { FactoryGirl.create(:vendor) }
  
  let(:estimate_requisition) { FactoryGirl.create(:estimate_requisition, job: job, author: author) }
  
  before do
  login_as(author)
  billa = FactoryGirl.create(:billable_entity, client: client)
  job = FactoryGirl.create(:job, client: client, author: author)
    estimate_requisition = FactoryGirl.create(:estimate_requisition, job: job, author: author, vendor: vendor)
    assign_role!(author, :finance, client)
    
  visit job_estimate_requisition_path(job, estimate_requisition)
  end
  
  scenario "successfully" do
    click_link "Delete Estimate requisition"
    
  expect(page).to have_content "Estimate requisition was deleted"
  end
end
