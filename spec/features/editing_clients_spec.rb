require "rails_helper"

RSpec.feature "Users can edit clients" do
  let(:user) { FactoryGirl.create(:user) }
  before do
    client = FactoryGirl.create(:client, name: "Uni Lever")
    login_as(user)
    assign_role!(user, :manager, client)
    visit "/"
    click_link "Uni Lever"
    click_link "Edit Client"
  end
  
  scenario "successfully" do
    fill_in "Name", with: "Unilever Revised"
    click_button "Update Client"

    expect(page).to have_content "Client was updated"
    expect(page).to have_content "Unilever Revised"
  end
  
  scenario "but not without valid entries" do
    fill_in "Name", with: ""
    click_button "Update Client"
    
    expect(page).to have_content "Client was not updated"
  end
end