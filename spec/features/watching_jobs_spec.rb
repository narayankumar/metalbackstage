require "rails_helper"

RSpec.feature "Users can watch and unwatch jobs" do
  let(:user) { FactoryGirl.create(:user) }
  let(:client) { FactoryGirl.create(:client) }
  let(:job) do
    FactoryGirl.create(:job, client: client, author: user)
  end
  
  before do
    assign_role!(user, "viewer", client)
    login_as(user)
    visit client_job_path(client, job)
  end
  
  scenario "successfully" do
    within("#watchers") do
      expect(page).to have_content user.email
    end
    
    click_link "Unwatch"
    expect(page).to have_content "You are no longer watching this " + "job"
    
    within("#watchers") do
      expect(page).to_not have_content user.email
    end
    
    click_link "Watch"
    expect(page).to have_content "You are now watching this job"
    
    within("#watchers") do
      expect(page).to have_content user.email
    end
  end
end
