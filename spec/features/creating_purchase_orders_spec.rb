require "rails_helper"

RSpec.feature "Users can create purchase orders" do
  let(:author) { FactoryGirl.create(:user, :admin) }
  let(:client) { FactoryGirl.create(:client) }
  
  
  before do
    login_as(author)
    job = FactoryGirl.create(:job, client: client, author: author)
    vendor = FactoryGirl.create(:vendor, name: "Example vendor")
    billable_entity = FactoryGirl.create(:billable_entity, client: client)
    estimate = FactoryGirl.create(:estimate, job: job, author: author, billable_entity: billable_entity)
    assign_role!(author, :handler, client)
    
    visit client_job_estimate_path(client, job, estimate)
    click_link "New Purchase Order"
  end
  
  scenario "with valid attributes" do
    fill_in "Title", with: "Sample purchase order"
    click_button "Create Purchase order"

    expect(page).to have_content "Purchase order has been created"
    expect(page).to have_content "Author: #{author.email}"
  end
  
  scenario "unless using invalid attributes" do
    click_button "Create Purchase order"
    expect(page).to have_content "Purchase order was not created"
    expect(page).to have_content "Title can't be blank"
  end
end
