require "rails_helper"

RSpec.feature "Users can create estimates" do
  let(:author) { FactoryGirl.create(:user, :admin) }
  let(:client) { FactoryGirl.create(:client) }
  
  before do
    login_as(author)
    billa = FactoryGirl.create(:billable_entity, client: client)
    job = FactoryGirl.create(:job, client: client, author: author)
    
    visit client_job_path(client, job)
    click_link "New Estimate"
  end
  
  scenario "with valid attributes" do
    fill_in "Name", with: "Sample Estimate"
    fill_in "Description", with: "Sample Estimate details"
    click_button "Create Estimate"

    expect(page).to have_content "Estimate has been created"
    expect(page).to have_content "Author: #{author.email}"
  end
  
  scenario "unless using invalid attributes" do
    click_button "Create Estimate"
    expect(page).to have_content "Estimate was not created"
    expect(page).to have_content "Name can't be blank"
    expect(page).to have_content "Description can't be blank"
  end
end
