require "rails_helper"

RSpec.feature "Users can delete invoice requisitions" do
  let(:author) { FactoryGirl.create(:user, :admin) }
  let(:client) { FactoryGirl.create(:client) }
  let(:billa) { FactoryGirl.create(:billable_entity, client: client) }
  let(:job) { FactoryGirl.create(:job, client: client) }
  
  before do
    login_as(author)
    estimate = FactoryGirl.create(:estimate, job: job, author: author, billable_entity: billa)
    invoice_requisition = FactoryGirl.create(:invoice_requisition, estimate: estimate, author: author)
    assign_role!(author, :manager, client)
  
    visit estimate_invoice_requisition_path(estimate, invoice_requisition)
  end
  
  scenario "successfully" do
    click_link "Delete Invoice requisition"
    
    expect(page).to have_content "Invoice requisition was deleted"
  end
end