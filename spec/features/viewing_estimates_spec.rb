require "rails_helper"

RSpec.feature "Users can view estimates" do
  before do
    author = FactoryGirl.create(:user, :admin)
    unilever = FactoryGirl.create(:client, name: "Uni Lever")
    samplejob = FactoryGirl.create(:job, client: unilever, name: "Sample Job", description: "Text for a sample job", author: author)
    FactoryGirl.create(:estimate, job: samplejob, name: "Sample Estimate", description: "Text for a sample estimate")
    login_as(author)
    assign_role!(author, :handler, unilever)
    visit "/"
  end
  
  scenario "for a given job" do
    click_link "Uni Lever"
    click_link "Sample Job"

    expect(page).to have_content "Sample Estimate"
  end
end