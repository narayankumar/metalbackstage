require "rails_helper"

RSpec.feature "Users can delete estimates" do
  let(:author) { FactoryGirl.create(:user, :admin) }
  let(:client) { FactoryGirl.create(:client) }
  
  before do
    login_as(author)
    
  billa = FactoryGirl.create(:billable_entity, client: client)
  job = FactoryGirl.create(:job, client: client, author: author)
    estimate = FactoryGirl.create(:estimate, job: job, author: author, billable_entity: billa)
  
    visit client_job_estimate_path(client, job, estimate)
  end
  
  scenario "successfully" do
    click_link "Delete Estimate"
    
    expect(page).to have_content "Estimate was deleted"
  end
end




