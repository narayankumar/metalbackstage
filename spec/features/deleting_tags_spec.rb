require "rails_helper"

RSpec.feature "Users can delete unwanted tags from job" do
  let(:user) { FactoryGirl.create(:user) }
  let(:client) { FactoryGirl.create(:client) }
  let(:job) do
    FactoryGirl.create(:job, client: client, tag_names: "ThisTagMustDie", author: user)
  end
  
  before do
    login_as(user)
    assign_role!(user, :manager, client)
    visit client_job_path(client, job)
  end
  
  scenario "successfully", js: true do
    within tag("ThisTagMustDie") do
      click_link "remove"
    end
    
    expect(page).to_not have_content "ThisTagMustDie"
  end
end