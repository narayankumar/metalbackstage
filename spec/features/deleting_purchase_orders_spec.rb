require "rails_helper"

RSpec.feature "Users can delete purchase orders" do
  let(:author) { FactoryGirl.create(:user, :admin) }
  let(:client) { FactoryGirl.create(:client) }
  let(:billa) { FactoryGirl.create(:billable_entity, client: client) }
  let(:job) { FactoryGirl.create(:job, client: client) }
  let(:vendor) { FactoryGirl.create(:vendor) }
  
  before do
    login_as(author)
    estimate = FactoryGirl.create(:estimate, job: job, author: author, billable_entity: billa)
    purchase_order = FactoryGirl.create(:purchase_order, estimate: estimate, author: author, vendor: vendor)
    
    assign_role!(author, :handler, client)
    visit estimate_purchase_order_path(estimate, purchase_order)
  end
  
  scenario "successfully" do
    click_link "Delete PO"
    
    expect(page).to have_content "Purchase order was deleted"
  end
end