require "rails_helper"

RSpec.feature "Users can delete invoices" do
  let(:author) { FactoryGirl.create(:user, :admin) }
  let(:client) { FactoryGirl.create(:client) }
  let(:billable_entity) { FactoryGirl.create(:billable_entity) }
  
  before do
    login_as(author)
    job = FactoryGirl.create(:job, client: client)
    estimate = FactoryGirl.create(:estimate, job: job, author: author, billable_entity: billable_entity)
    invoice = FactoryGirl.create(:invoice, estimate: estimate, author: author)
    assign_role!(author, :director, client)
  
    visit estimate_invoice_path(estimate, invoice)
  end
  
  scenario "successfully" do
    click_link "Delete Invoice"
    
    expect(page).to have_content "Invoice was deleted"
  end
end