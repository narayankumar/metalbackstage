require "rails_helper"

RSpec.feature "Users can edit unbillable purchase orders" do
  let(:author) { FactoryGirl.create(:user, :admin)}
  let(:client) { FactoryGirl.create(:client) }
  let(:vendor) { FactoryGirl.create(:vendor) }
  let(:unbillable_purchase_order) { FactoryGirl.create(:unbillable_purchase_order, client: client, author: author, vendor: vendor) } 
  
  before do
    assign_role!(author, :finance, client)
    login_as(author)
    visit client_unbillable_purchase_order_path(client, unbillable_purchase_order)
    click_link "Edit NBPO"
  end
  
  scenario "with valid attributes" do
    fill_in "Amount", with: "1000"
    fill_in "Details", with: "this and that and that"
    click_button "Update Unbillable purchase order"
    
    expect(page).to have_content "Unbillable purchase order was updated"
    
    within("#unbillable_purchase_orders") do
      expect(page).to have_content "this and that and that"
      expect(page).not_to have_content unbillable_purchase_order.amount
    end
  end
  
  scenario "but no invalid entries" do
    fill_in "Amount", with: ""
    click_button "Update Unbillable purchase order"
    
    expect(page).to have_content "Unbillable purchase order was not updated"
    expect(page).to have_content "Amount can't be blank"
  end
end


