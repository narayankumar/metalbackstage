require 'rails_helper'

RSpec.describe AttachmentPolicy do
  context "permissions" do
    subject { AttachmentPolicy.new(user, attachment) }
    let(:user) { FactoryGirl.create(:user) }
    let(:client) { FactoryGirl.create(:client) }
    let(:job) { FactoryGirl.create(:job, client: client) }
    let(:attachment) { FactoryGirl.create(:attachment, job: job) }
    
    context "for anonymous users" do
      let(:user) { nil }
      it { should_not permit_action :show }
    end
    
    context "for members" do
      before { assign_role!(user, :member, client) }
      it { should permit_action :show }
    end
    
    context "for handlers" do
      before { assign_role!(user, :handler, client) }
      it { should permit_action :show }
    end
    
    context "for managers" do
      before { assign_role!(user, :manager, client) }
      it { should permit_action :show }
      it { should permit_action :destroy }
    end
    
    context "for finance" do
      before { assign_role!(user, :finance, client) }
      it { should permit_action :show }
    end
    
    context "for directors" do
      before { assign_role!(user, :director, client) }
      it { should permit_action :show }
    end
    
    context "for managers of other clients" do
      before do
        assign_role!(user, :manager, FactoryGirl.create(:client))
      end
      it { should_not permit_action :show }
    end
    
    context "for admins" do
      let(:user) { FactoryGirl.create :user, :admin }
      it { should permit_action :show }
      it { should permit_action :destroy }
    end
  end
end
