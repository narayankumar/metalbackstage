require 'rails_helper'

RSpec.describe JobPolicy do
  context "permissions" do
    subject { JobPolicy.new(user, job) }
    
    let(:user) { FactoryGirl.create(:user) }
    let(:client) { FactoryGirl.create(:client) }
    let(:job) { FactoryGirl.create(:job, client: client) }
    
    context "for anonymous users" do
      let(:user) { nil }
      
      it { should_not permit_action :show }
      it { should_not permit_action :create }
      it { should_not permit_action :update }
      it { should_not permit_action :destroy }
    end
    
    context "for members of client" do
      before { assign_role!(user, :member, client) }
      
      it { should permit_action :show }
      it { should_not permit_action :create }
      it { should_not permit_action :update }
      it { should_not permit_action :destroy }
    end
    
    context "for handlers of client" do
      before { assign_role!(user, :handler, client) }
      
      it { should permit_action :show }
      it { should permit_action :create }
      it { should_not permit_action :update }
      context "when handler created job" do
        before { job.author = user }
        it { should permit_action :update }
      end
      it { should_not permit_action :destroy }
    end
    
    context "for managers of client" do
      before { assign_role!(user, :manager, client) }
      
      it { should permit_action :show }
      it { should permit_action :create }
      it { should permit_action :update }
      it { should permit_action :destroy }
    end
    
    context "for finance persons of client" do
      before { assign_role!(user, :finance, client) }
      
      it { should permit_action :show }
      it { should_not permit_action :create }
      it { should_not permit_action :update }
      it { should_not permit_action :destroy }
    end
    
    context "for directors of client" do
      before { assign_role!(user, :director, client) }
      
      it { should permit_action :show }
      it { should permit_action :create }
      it { should permit_action :update }
      it { should permit_action :destroy }
    end
    
    context "for managers of other clients" do
      before do
        assign_role!(user, :manager, FactoryGirl.create(:client))
      end
      
      it { should_not permit_action :show }
      it { should_not permit_action :create }
      it { should_not permit_action :update }
      it { should_not permit_action :destroy }
    end
    
    context "for admins" do
      let(:user) { FactoryGirl.create :user, :admin }
      
      it { should permit_action :show }
      it { should permit_action :create }
      it { should permit_action :update }
      it { should permit_action :destroy }
    end   
  end
end
