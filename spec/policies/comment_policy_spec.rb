require "rails_helper"

RSpec.describe CommentPolicy do
  context "permissions" do
    subject { CommentPolicy.new(user, comment) }

    let(:user) { FactoryGirl.create(:user) }
    let(:client) { FactoryGirl.create(:client) }
    let(:job) { FactoryGirl.create(:job, client: client) }
    let(:comment) { FactoryGirl.create(:comment, job: job) }

    context "for anonymous users" do
      let(:user) { nil }
      it { should_not permit_action :create }
    end

    context "for members of client" do
      before { assign_role!(user, :member, client) }
      it { should permit_action :create }
    end

    context "for handlers of client" do
      before { assign_role!(user, :handler, client) }
      it { should permit_action :create }
    end

    context "for managers of client" do
      before { assign_role!(user, :manager, client) }
      it { should permit_action :create }
    end

    context "for directors of client" do
      before { assign_role!(user, :director, client) }
      it { should permit_action :create }
    end

    context "for finance members of client" do
      before { assign_role!(user, :finance, client) }
      it { should permit_action :create }
    end

    context "for managers of other clients" do
      before do
        assign_role!(user, :manager, FactoryGirl.create(:client))
      end
      it { should_not permit_action :create }
    end

    context "for admins" do
      let(:user) { FactoryGirl.create :user, :admin }
      it { should permit_action :create }
    end
  end
end
