require 'rails_helper'

describe ClientPolicy do

  let(:user) { User.new }

  subject { ClientPolicy }
  
  context "policy_scope" do
    subject { Pundit.policy_scope(user, Client) }
    
    let!(:client) { FactoryGirl.create :client }
    let(:user) { FactoryGirl.create :user }
    
    it "is empty for anonymous users" do
      expect(Pundit.policy_scope(nil, Client)).to be_empty
    end
    
    it "includes clients a user is allowed to view" do
      assign_role!(user, :member, client)
      expect(subject).to include(client)
    end
    
    it "doesn't include clients a user isn't allowed to view" do
      expect(subject).to be_empty
    end
    
    it "returns all clients for admins" do
      user.admin = true
      expect(subject).to include(client)
    end
  end

  context "permissions" do
    subject { ClientPolicy.new(user, client) }
    let(:user) { FactoryGirl.create(:user) }
    let(:client) { FactoryGirl.create(:client) }
    
    context "for anonymous users" do
      let(:user) { nil }
      
      it { should_not permit_action :show }
      it { should_not permit_action :update }
    end
    
    context "for members of client" do
      before { assign_role!(user, :member, client) }
      
      it { should permit_action :show }
      it { should_not permit_action :update }
    end
    
    context "for handlers of client" do
      before { assign_role!(user, :handler, client) }
      
      it { should permit_action :show }
      it { should_not permit_action :update }
    end
    
    context "for managers of client" do
      before { assign_role!(user, :manager, client) }
      
      it { should permit_action :show }
      it { should permit_action :update }
    end
    
    context "for finance persons of client" do
      before { assign_role!(user, :finance, client) }
      
      it { should permit_action :show }
      it { should_not permit_action :update }
    end
    
    context "for directors of client" do
      before { assign_role!(user, :director, client) }
      
      it { should permit_action :show }
      it { should_not permit_action :update }
    end
    
    context "for managers of other clients" do
      before do
        assign_role!(user, :manager, FactoryGirl.create(:client))
      end
      
      it { should_not permit_action :show }
      it { should_not permit_action :update }
    end
    
    context "for admins" do
      let(:user) { FactoryGirl.create :user, :admin }
      
      it { should permit_action :show }
      it { should permit_action :update}
    end
  end
end
