require 'rails_helper'

RSpec.describe EstimateRequisitionPolicy do
  context "permissions" do
    subject { EstimateRequisitionPolicy.new(user, estimate_requisition) }
    
    let(:user) { FactoryGirl.create(:user) }
    let(:client) { FactoryGirl.create(:client) }
    let(:job) { FactoryGirl.create(:job, client: client) }
    let(:estimate_requisition) { FactoryGirl.create(:estimate_requisition, job: job)}
    
    context "for anonymous users" do
      let(:user) { nil }
      
      it { should_not permit_action :show }
      it { should_not permit_action :create }
      it { should_not permit_action :update }
      it { should_not permit_action :destroy }
    end
    
    context "for members of client" do
      before { assign_role!(user, :member, client) }
      
      it { should_not permit_action :show }
      it { should_not permit_action :create }
      it { should_not permit_action :update }
      it { should_not permit_action :destroy }
    end
    
    context "for handlers of client" do
      before { assign_role!(user, :handler, client) }
      
      it { should permit_action :show }
      it { should permit_action :create }
      it { should_not permit_action :update }
      it { should_not permit_action :destroy }
    end
    
    context "for managers of client" do
      before { assign_role!(user, :manager, client) }
      
      it { should permit_action :show }
      it { should permit_action :create }
      it { should_not permit_action :update }
      it { should_not permit_action :destroy }
    end
    
    context "for directors of client" do
      before { assign_role!(user, :director, client) }
      
      it { should permit_action :show }
      it { should_not permit_action :create }
      it { should_not permit_action :update }
      it { should_not permit_action :destroy }
    end
    
    context "for managers of other clients" do
      before do
        assign_role!(user, :manager, FactoryGirl.create(:client))
      end
      
      it { should_not permit_action :show }
      it { should_not permit_action :create }
      it { should_not permit_action :update }
      it { should_not permit_action :destroy }
    end
    
    context "for admins" do
      let(:user) { FactoryGirl.create :user, :admin }
      
      it { should permit_action :show }
      it { should permit_action :create }
      it { should permit_action :update }
      it { should permit_action :destroy }
    end
    
    context "for finance" do
      let(:user) { FactoryGirl.create :user, :finance }
    
      it { should permit_action :show }
      it { should_not permit_action :create }
      it { should_not permit_action :update }
      it { should permit_action :destroy }
    end
  end
end