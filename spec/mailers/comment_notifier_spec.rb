require "rails_helper"

RSpec.describe CommentNotifier, type: :mailer do
  describe "created" do
    let(:client) { FactoryGirl.create(:client) }
    let(:job_owner) { FactoryGirl.create(:user) }
    let(:job) do
      FactoryGirl.create(:job, client: client, author: job_owner)
    end
    
    let(:commenter) { FactoryGirl.create(:user) }
    let(:comment) do
      Comment.new(job: job, author: commenter, text: "Test comment")
    end
    
    let(:email) do 
      CommentNotifier.created(comment, job_owner)
    end
    
    it "sends out an email notification about a new comment" do
      expect(email.to).to include job_owner.email
      title = "#{job.name} for #{client.name} has been updated."
      expect(email.body.to_s).to include title
      expect(email.body.to_s).to include "#{commenter.email} wrote:"
      expect(email.body.to_s).to include comment.text
    end
  end
end