FactoryGirl.define do
  factory :purchase_order do
    title "Example purchase order"
    amount "900"
    vendor_id "1"
    bill_given_status "Not given to Accounts"
    client_paid_status "Not paid"
    vendor_paid_status "Not paid"
  end
end