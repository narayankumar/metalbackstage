FactoryGirl.define do
  factory :job do
    name "Example job"
    description "Example text for example job"
    brief_date "01/02/2014"
    deadline "01/02/2014"
  end
end