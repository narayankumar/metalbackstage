FactoryGirl.define do
  factory :estimate do
    name "Example Estimate"
    description "Example text for example estimate"
    billable_entity_id "1"
    estimate_type "ART"
  end
end