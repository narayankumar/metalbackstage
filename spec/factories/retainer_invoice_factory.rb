FactoryGirl.define do
  factory :retainer_invoice do
    title "Example Retainer Invoice"
    narration "Example retainer invoice narration"
    billable_entity_id "1"
  end
end
