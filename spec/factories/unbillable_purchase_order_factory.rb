FactoryGirl.define do
  factory :unbillable_purchase_order do
    details "Example text for unbillable_purchase_order"
    amount "5000"
    vendor_id "1"
  end
end