FactoryGirl.define do
  factory :billable_entity do
    name "Example Billable Entity"
    phone "09098947"
  end
end