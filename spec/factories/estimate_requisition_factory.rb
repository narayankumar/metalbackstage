FactoryGirl.define do
  factory :estimate_requisition do
    amount "3000"
    details "Example text for example estimate reqn"
    vendor_id "1"
  end
end