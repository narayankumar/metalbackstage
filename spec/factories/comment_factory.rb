FactoryGirl.define do
  factory :comment do
    text { "A comment describing some changes that should be made to this job." }
  end
end