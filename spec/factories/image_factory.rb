FactoryGirl.define do
  factory :image do
    transient do
      image_to_attach "spec/fixtures/ballpen.jpg"
    end
    
    file { File.open image_to_attach }
  end
end
