FactoryGirl.define do
  factory :invoice do
    title "Example Invoicing"
    details "Example text for example invoice"
    billable_entity_id "1"
    invoice_type "ART"
  end
end