class UnbillablePurchaseOrderPdf < Prawn::Document
  def initialize(unbillable_purchase_order, view)
    super()
    @unbillable_purchase_order = unbillable_purchase_order
    @view = view
    
    logo
    date
    address
    details
  end
  
  def logo
    logopath = "#{Rails.root}/app/assets/images/METAL-LOGO.jpg"
    image logopath, :width => 180, :height => 32
    move_down 10
    text "3, Sarkar Heritage, Kane Road, Bandstand, Bandra West, Mumbai 400 050. Tel: 91 22 6170 2600", size: 10
    move_down 30
    text "P U R C H A S E  O R D E R", size: 18
  end
  
  def date
    move_down 30
    text "#{@unbillable_purchase_order.created_at.strftime("%d/%m/%Y")}", :size => 12
  end
  
  def address
    move_down 10
    text "PO No.: #{@unbillable_purchase_order.serial_number}"
    move_down 10
    text "#{@unbillable_purchase_order.client.name}", style: :bold
  end
  
  def details
    move_down 20
    text "Vendor: #{precision(@unbillable_purchase_order.vendor.name)}", style: :bold
    move_down 10
    text "#{precision(@unbillable_purchase_order.vendor.address)}"
    move_down 10
    text "Details: #{precision(@unbillable_purchase_order.details)}"
    move_down 10
    text "Amount: INR #{precision(@unbillable_purchase_order.amount)}", style: :bold  
    move_down 10   
  end
  
  def precision(num)
    @view.number_with_precision(num, :precision => 2)
  end
end