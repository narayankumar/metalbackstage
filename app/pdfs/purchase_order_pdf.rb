class PurchaseOrderPdf < Prawn::Document
  def initialize(purchase_order, view)
    super()
    @purchase_order = purchase_order
    @view = view
    
    logo
    date
    address
    details
  end
  
  def logo
    logopath = "#{Rails.root}/app/assets/images/METAL-LOGO.jpg"
    image logopath, :width => 180, :height => 32
    move_down 10
    text "3, Sarkar Heritage, Kane Road, Bandstand, Bandra West, Mumbai 400 050. Tel: 91 22 6170 2600", size: 10
    move_down 30
    text "P U R C H A S E  O R D E R", size: 18
  end
  
  def date
    move_down 30
    text "#{@purchase_order.created_at.strftime("%d/%m/%Y")}", :size => 12
  end
  
  def address
    move_down 10
    text "PO No.: #{@purchase_order.serial_number}"
    move_down 10
    text "#{@purchase_order.estimate.job.client.name}", size: 16
    move_down 10
    text "#{@purchase_order.vendor.name}", style: :bold
    move_down 10
    text "#{@purchase_order.vendor.address}"
  end
  
  def details
    move_down 20
    text "#{@purchase_order.title}", style: :bold
    move_down 10
    text "#{@purchase_order.details}"
    move_down 10
    text "Amount: INR #{precision(@purchase_order.amount)}", style: :bold
    move_down 10
    text "(#{@purchase_order.amount_in_words})"
    move_down 10
    text "Terms: #{precision(@purchase_order.payment_terms)}", size: 10
    move_down 30
  end
  
  def precision(num)
    @view.number_with_precision(num, :precision => 2)
  end
end