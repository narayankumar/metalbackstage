class EstimatePdf < Prawn::Document
  def initialize(estimate, view)
    super()
    @estimate = estimate
    @view = view
    
    logo
    date
    address
    details
    statutory
  end
  
  def logo
    logopath = "#{Rails.root}/app/assets/images/METAL-LOGO.jpg"
    image logopath, :width => 180, :height => 32
    move_down 10
    text "3, Sarkar Heritage, Kane Road, Bandstand, Bandra West, Mumbai 400 050. Tel: 91 22 6170 2600", size: 10
    move_down 30
    text "E S T I M A T E", size: 18
  end
  
  def date
    move_down 30
    text "#{@estimate.printer_date.strftime("%d/%m/%Y")}", :size => 12
  end
  
  def address
    move_down 10
    text "Estimate No.: #{@estimate.serial_number}"
    move_down 10
    text "#{@estimate.job.client.name}", style: :bold
    move_down 10
    text "#{@estimate.billable_entity.name}"
    move_down 10
    text "#{@estimate.billable_entity.address}"
  end
  
  def details
    move_down 20
    text "#{@estimate.name}", style: :bold
    move_down 10
    text "#{@estimate.description}"
    move_down 10
    text "INR #{precision(@estimate.amount)}", style: :bold
    move_down 10
    if !@estimate.vendor.nil? && @estimate.vendor.name != "I N T E R N A L"
      text "Vendor: #{@estimate.vendor.name}"
    end
  end
  
  def statutory
    move_down 30
    text "Notes: ", :size => 10, style: :bold
    move_down 10
    text "#{@estimate.notes}", :size => 10
    move_down 10
    text "#{@estimate.signoff}", :size => 12
    move_down 40
    text "#{@estimate.signatory}"
    move_down 20
    text "#{@estimate.client_signatory}"
  end
  
  def precision(num)
    @view.number_with_precision(num, :precision => 2)
  end
end