class RetainerInvoicePdf < Prawn::Document
  def initialize(retainer_invoice, view)
    super()
    @retainer_invoice = retainer_invoice
    @view = view
    
    logo
    date
    address
    details
    statutory
  end
  
  def logo
    logopath = "#{Rails.root}/app/assets/images/METAL-LOGO.jpg"
    image logopath, :width => 180, :height => 32
    move_down 10
    text "3, Sarkar Heritage, Kane Road, Bandstand, Bandra West, Mumbai 400 050. Tel: 91 22 6170 2600", size: 10
    move_down 30
    text "#{@retainer_invoice.printer_title}", size: 18
  end
  
  def date
    move_down 30
    text "#{@retainer_invoice.printer_date.strftime("%d/%m/%Y")}", :size => 12
  end
  
  def address
    move_down 10
    text "Invoice No.: #{@retainer_invoice.serial_number}"
    move_down 10
    text "#{@retainer_invoice.client.name}", style: :bold
    move_down 10
    text "#{@retainer_invoice.billable_entity.name}"
    move_down 10
    text "#{@retainer_invoice.billable_entity.address}"
  end
  
  def details
    move_down 20
    text "#{@retainer_invoice.title}", style: :bold
    move_down 10
    text "Amount: INR #{precision(@retainer_invoice.amount)}", style: :bold
    move_down 10
    text "Service Tax: INR #{precision(@retainer_invoice.service_tax)}"
    move_down 10
    text "Swachh Bharat Cess: INR #{precision(@retainer_invoice.swachh_bharat)}"
    move_down 10
    text "Krishi Kalyan Cess: INR #{precision(@retainer_invoice.krishi_kalyan_cess)}"
    move_down 10
    text "Total Amount: INR #{precision(@retainer_invoice.total_amount)}", style: :bold
    move_down 10
    text "(#{@retainer_invoice.amount_in_words})"
  end
  
  def statutory
    move_down 30
    text "Notes: ", :size => 10, style: :bold
    move_down 10
    text "#{@retainer_invoice.notes}", :size => 10
    move_down 10
    text "#{@retainer_invoice.signoff}", :size => 12
    move_down 40
    text "#{@retainer_invoice.signatory}"
    move_down 20
  end
  
  def precision(num)
    @view.number_with_precision(num, :precision => 2)
  end
end