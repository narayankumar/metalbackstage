class InvoicePdf < Prawn::Document
  def initialize(invoice, view)
    super()
    @invoice = invoice
    @view = view
    
    logo
    date
    address
    details
    statutory
  end
  
  def logo
    logopath = "#{Rails.root}/app/assets/images/METAL-LOGO.jpg"
    image logopath, :width => 180, :height => 32
    move_down 10
    text "3, Sarkar Heritage, Kane Road, Bandstand, Bandra West, Mumbai 400 050. Tel: 91 22 6170 2600", size: 10
    move_down 30
    text "I N V O I C E", size: 18
  end
  
  def date
    move_down 30
    text "#{@invoice.printer_date.strftime("%d/%m/%Y")}", :size => 12
  end
  
  def address
    move_down 10
    text "Invoice No.: #{@invoice.serial_number}"
    move_down 10
    text "#{@invoice.estimate.job.client.name}", style: :bold
    move_down 10
    text "#{@invoice.estimate.billable_entity.name}"
    move_down 10
    text "#{@invoice.estimate.billable_entity.address}"
  end
  
  def details
    move_down 20
    text "#{@invoice.title}", style: :bold
    move_down 10
    text "#{@invoice.details}"
    move_down 10
    text "Amount: INR #{precision(@invoice.amount)}", style: :bold
    move_down 10
    text "Service Tax: INR #{precision(@invoice.service_tax)}"
    move_down 10
    text "Swachh Bharat Cess: INR #{precision(@invoice.swachh_bharat)}"
    move_down 10
    text "Krishi Kalyan Cess: INR #{precision(@invoice.krishi_kalyan_cess)}"
    move_down 10
    text "Total Amount: INR #{precision(@invoice.total_amount)}", style: :bold
    move_down 10
    text "(#{@invoice.amount_in_words})"
  end
  
  def statutory
    move_down 30
    text "Notes: ", :size => 10, style: :bold
    move_down 10
    text "#{@invoice.notes}", :size => 10
    move_down 10
    text "#{@invoice.signoff}", :size => 12
    move_down 40
    text "#{@invoice.signatory}"
    move_down 20
    text "#{@invoice.client_signatory}"
  end
  
  def precision(num)
    @view.number_with_precision(num, :precision => 2)
  end
end