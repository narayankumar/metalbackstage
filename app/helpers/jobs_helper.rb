module JobsHelper
  def status_transition_for(comment)
    if comment.previous_status != comment.status
      content_tag(:p) do
        value = "<strong><i class='fa fa-gear'></i> status change:</strong>"
        if comment.previous_status.present?
          value += " #{render comment.previous_status}"
        end
        value += " to #{render comment.status}"
        value.html_safe
      end
    end
  end
  
  def toggle_watching_button(job)
    text = if job.watchers.include?(current_user)
      "Unwatch"
           else
      "Watch"
           end
    link_to text, watch_client_job_path(job.client, job), class: text.parameterize, method: :post
  end
end
