module ApplicationHelper
  def title(*parts)
    unless parts.empty?
      content_for :title do
        (parts << "Metal Backstage").join(" | ")
      end
    end
  end
  
  def admins_only(&block)
    block.call if current_user.try(:admin?)
  end
  
  def finance_only(&block)
    block.call if current_user.try(:finance?) || current_user.try(:admin?)
  end
end
