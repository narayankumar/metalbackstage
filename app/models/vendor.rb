class Vendor < ActiveRecord::Base
  belongs_to :ventype
  has_many :unbillable_purchase_orders
  has_many :purchase_orders
  has_many :estimates
  has_many :estimate_requisitions
  
  validates :name, presence: true
end
