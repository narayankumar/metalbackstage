class Image < ActiveRecord::Base
  belongs_to :job  
  mount_uploader :image, ImageUploader
end
