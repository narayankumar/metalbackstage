class Idea < ActiveRecord::Base
  belongs_to :job
  mount_uploader :idea, IdeaUploader
end
