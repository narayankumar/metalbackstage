class Ventype < ActiveRecord::Base
  has_many :vendors, dependent: :destroy
  
  validates :name, presence: true
end
