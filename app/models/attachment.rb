class Attachment < ActiveRecord::Base
  belongs_to :job
  
  mount_uploader :file, AttachmentUploader
end
