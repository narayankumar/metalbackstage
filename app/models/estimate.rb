class Estimate < ActiveRecord::Base
  belongs_to :job
  belongs_to :vendor
  belongs_to :billable_entity
  belongs_to :author, class_name: "User"
  
  has_many :invoices, dependent: :destroy
  has_many :invoice_requisitions, dependent: :destroy
  has_many :purchase_orders, dependent: :destroy
  has_many :estfiles, dependent: :destroy
  accepts_nested_attributes_for :estfiles, reject_if: :all_blank
  
  validates :name, presence: true
  validates :description, presence: true
  
  before_create :set_serial_number
  after_create :set_final
  
  before_update :set_final_update, :if => :estimate_type_changed?
  
  def set_serial_number
    self.serial_number = "/#{created_at.month}/#{created_at.year}/"
  end
  
  def set_final
    num = "#{id}"
    num = num.to_i + 992
    self.serial_number = "#{estimate_type}" + self.serial_number + "#{num}"
    self.save
  end
  
  def set_final_update
    num = "#{id}"
    num = num.to_i + 992
    self.serial_number = "#{estimate_type}/#{created_at.month}/#{created_at.year}/#{num}"
  end
end
