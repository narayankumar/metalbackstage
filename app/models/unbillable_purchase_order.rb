class UnbillablePurchaseOrder < ActiveRecord::Base
  belongs_to :client
  belongs_to :vendor
  belongs_to :author, class_name: "User"
  
  validates :details, presence: true
  validates :amount, presence: true
  
  before_create :set_serial_number
  after_create :set_final
  
  after_update :notify_finance, :if => :approval_status_changed?
  
  private
  
  def set_serial_number
    self.serial_number = "NONBPO/#{created_at.month}/#{created_at.year}/"
  end
  
  def set_final
    num = "#{id}"
    num = num.to_i + 1000
    self.serial_number = self.serial_number + "#{num}"
    self.save
  end
  
  def notify_finance
      (User.where(finance: true)).each do |user|
        UnbillablePurchaseOrderNotifier.updated(self, user).deliver_now
      end
  end
end
