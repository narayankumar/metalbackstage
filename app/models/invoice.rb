class Invoice < ActiveRecord::Base
  belongs_to :estimate
  belongs_to :billable_entity
  belongs_to :author, class_name: "User"
  
  validates :title, presence: true
  validates :details, presence: true
  
  before_create :set_serial_number
  after_create :set_final
  
  before_update :set_final_update, :if => :invoice_type_changed?
  
  def set_serial_number
    self.serial_number = "/#{created_at.month}/#{created_at.year}/"
  end
  
  def set_final
    num = "#{id}"
    num = num.to_i + 997
    self.serial_number = "#{invoice_type}" + self.serial_number + "#{num}"
    self.save
  end
  
  def set_final_update
    num = "#{id}"
    num = num.to_i + 997
    self.serial_number = "#{invoice_type}/#{created_at.month}/#{created_at.year}/#{num}"
  end
end
