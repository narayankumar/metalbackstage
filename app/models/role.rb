class Role < ActiveRecord::Base
  belongs_to :user
  belongs_to :client
  
  def self.available_roles
    %w(director manager handler member)
  end
end
