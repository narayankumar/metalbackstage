class Job < ActiveRecord::Base
  belongs_to :client
  belongs_to :author, class_name: "User"
  belongs_to :status
  
  has_many :estimates, dependent: :destroy
  has_many :estimate_requisitions, dependent: :destroy
  has_many :attachments, dependent: :destroy
  has_many :images, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :ideas, dependent: :destroy
  has_and_belongs_to_many :tags, uniq: true
  has_and_belongs_to_many :watchers, join_table: "job_watchers", class_name: "User", uniq: true
  
  attr_accessor :tag_names
  
  accepts_nested_attributes_for :attachments, reject_if: :all_blank
  accepts_nested_attributes_for :images, reject_if: :all_blank
  accepts_nested_attributes_for :ideas, reject_if: :all_blank
  
  validates :name, presence: true
  validates :description, presence: true, length: { minimum: 10 }
  
  searcher do
    label :tag, from: :tags, field: "name"
  end
  
  before_create :set_serial_number
  before_create :set_default_status
  after_create :set_final
  after_create :author_watches_me
  
  def tag_names=(names)
    @tag_names = names
    names.split.each do |name|
      self.tags << Tag.find_or_initialize_by(name: name)
    end
  end
  
  private
  
  def set_serial_number
    self.serial_number = "AJ/#{created_at.month}/#{created_at.year}/"
  end
  
  def set_final
    num = "#{id}"
    num = num.to_i + 1000
    self.serial_number = self.serial_number + "#{num}"
    self.save
  end
  
  def set_default_status
    self.status_id = 1
  end
  
  def author_watches_me
    if author.present? && !self.watchers.include?(author)
      self.watchers << author
    end
  end
end
