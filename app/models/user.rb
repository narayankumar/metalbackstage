class User < ActiveRecord::Base
  has_many :roles
  has_many :leave_applications
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :recoverable, :rememberable, :trackable, :validatable
  
  scope :excluding_archived, lambda { where(archived_at: nil) }
  
  def to_s
    "#{email}"
  end
  
  def archive
    self.update(archived_at: Time.now)
  end
  
  def active_for_authentication?
    super && archived_at.nil?
  end
  
  def inactive_message
    archived_at.nil? ? super : :archived
  end
  
  def role_on(client)
    roles.find_by(client_id: client).try(:role)
  end
end
