class PurchaseOrder < ActiveRecord::Base
  belongs_to :estimate
  belongs_to :vendor
  belongs_to :author, class_name: "User"
  
  validates :title, presence: true
  
  before_create :set_serial_number
  after_create :set_final

  def set_serial_number
    self.serial_number = "PO/#{created_at.month}/#{created_at.year}/"
  end
  
  def set_final
    num = "#{id}"
    num = num.to_i + 1000
    self.serial_number = self.serial_number + "#{num}"
    self.save
  end
end
