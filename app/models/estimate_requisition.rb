class EstimateRequisition < ActiveRecord::Base
  belongs_to :job
  belongs_to :vendor
  belongs_to :author, class_name: "User"
  
  validates :details, presence: true
  
  mount_uploader :er_attach, ErAttachUploader
end
