class LeaveApplication < ActiveRecord::Base
  belongs_to :author, class_name: "User"
  
  validates :details, presence: true
  validates :leave_type, presence: true
  
  after_update :notify_finance, :if => :status_changed?
  after_update :update_user, :if => :status_changed?
  
  private
  
  def notify_finance
    (User.where(finance: true)).each do |user|
      LeaveApplicationNotifier.updated(self, user).deliver_now
    end
  end
  
  def update_user
    if self.status == "Approved"
      ltype = self.leave_type.to_s.split(" ").first.downcase
      leave = (self.to_date - self.from_date).to_i + 1
      oldbal = (self.author.send("#{ltype}")).to_i
      newbal = oldbal - leave
      if ltype == "casual"
        self.author.casual = newbal
        self.author.save!
      elsif ltype == "sick"
        self.author.sick = newbal
        self.author.save!
      else
        self.author.privileged = newbal
        self.author.save!
      end
    end
  end
end
