class Comment < ActiveRecord::Base
  belongs_to :job
  belongs_to :author, class_name: "User"
  belongs_to :status
  belongs_to :previous_status, class_name: "Status"
  
  attr_accessor :tag_names
  
  validates :text, presence: true
  before_create :set_previous_status
  after_create :set_job_status
  after_create :associate_tags_with_job
  after_create :author_watches_job
  
  scope :persisted, lambda { where.not(id: nil).order("id DESC") }
  
  private
  
  def set_job_status
    job.status = status
    job.save!
  end
  
  def set_previous_status
    self.previous_status = job.status
  end
  
  def associate_tags_with_job
    if tag_names
      tag_names.split.each do |name|
        job.tags << Tag.find_or_create_by(name: name)
      end
    end
  end
  
  def author_watches_job
    if author.present? && !job.watchers.include?(author)
      job.watchers << author
    end
  end
end
