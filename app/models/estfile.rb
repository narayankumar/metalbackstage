class Estfile < ActiveRecord::Base
  belongs_to :estimate
  
  mount_uploader :file, EstfileUploader
end
