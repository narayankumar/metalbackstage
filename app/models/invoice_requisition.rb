class InvoiceRequisition < ActiveRecord::Base
  belongs_to :estimate
  belongs_to :author, class_name: "User"
  
  validates :notes, presence: true
end
