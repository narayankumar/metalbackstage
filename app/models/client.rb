class Client < ActiveRecord::Base
  has_many :jobs, dependent: :delete_all
  has_many :billable_entities, dependent: :destroy
  has_many :unbillable_purchase_orders, dependent: :destroy
  has_many :retainer_invoices, dependent: :destroy
  has_many :roles, dependent: :delete_all
  
  validates :name, presence: true
  
  def has_belonger?(user)
    roles.exists?(user_id: user)
  end
  
  [:director, :finance, :manager, :handler, :member].each do |role|
    define_method "has_#{role}?" do |user|
      roles.exists?(user_id: user, role: role)
    end
  end
end
