class BillableEntity < ActiveRecord::Base
  belongs_to :client
  has_many :retainer_invoices
  has_many :estimates
  has_many :invoices
  
  validates :name, presence: true
end
