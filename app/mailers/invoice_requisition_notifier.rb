class InvoiceRequisitionNotifier < ApplicationMailer
  def created(invoice_requisition, user)
    @invoice_requisition = invoice_requisition
    @user = user
    @estimate = @invoice_requisition.estimate
    @job = @estimate.job
    @client = @job.client
    
    subject = "[metalbackstage] Invoice Requisition #{@client.name} #{@job.name}"
    mail(to: user.email, subject: subject)
  end
end