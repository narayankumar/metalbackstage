class EstimateRequisitionNotifier < ApplicationMailer
  def created(estimate_requisition, user)
    @estimate_requisition = estimate_requisition
    @user = user
    @job = estimate_requisition.job
    @client = @job.client
    
    subject = "[metalbackstage] Estimate Requisition: #{@client.name} #{@job.name}"
    mail(to: user.email, subject: subject)
  end
end
