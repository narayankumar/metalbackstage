class LeaveApplicationNotifier < ApplicationMailer
  def created(leave_application, user)
    @leave_application = leave_application
    @user = user
    
    subject = "[metalbackstage] Leave application from #{@leave_application.author}"
    mail(to: user.email, subject: subject)
  end
  
  def updated(leave_application, user)
    @leave_application = leave_application
    @user = user
    
    subject = "[metalbackstage] Leave application: #{@leave_application.author}"
    mail(to: user.email, subject: subject)
  end
end