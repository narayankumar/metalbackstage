class CommentNotifier < ApplicationMailer
  def created(comment, user)
    @comment = comment
    @user = user
    @job = comment.job
    @client = @job.client
    
    subject = "[backstage] #{@client.name} - #{@job.name}"
    mail(to: user.email, subject: subject)
  end
end
