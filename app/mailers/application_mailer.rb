class ApplicationMailer < ActionMailer::Base
  default from: "admin@metalcomm.com"
  layout 'mailer'
end
