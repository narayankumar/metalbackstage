class UnbillablePurchaseOrderNotifier < ApplicationMailer
  def created(unbillable_purchase_order, user)
    @unbillable_purchase_order = unbillable_purchase_order
    @user = user
    @client = @unbillable_purchase_order.client
    
    subject = "[metalbackstage] NBPO: #{@client.name}"
    mail(to: user.email, subject: subject)
  end
  
  def updated(unbillable_purchase_order, user)
    @unbillable_purchase_order = unbillable_purchase_order
    @user = user
    @client = @unbillable_purchase_order.client
    
    subject = "[metalbackstage] NBPO Approval: #{@client.name}"
    mail(to: user.email, subject: subject)
  end
end