class JobPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end
  
  def show?
    user.try(:admin?) || record.client.has_belonger?(user)
  end
  
  def create?
    user.try(:admin?) || record.client.has_manager?(user) || record.client.has_handler?(user) || record.client.has_director?(user)
  end
  
  def update?
    user.try(:admin?) || record.client.has_manager?(user) || (record.client.has_handler?(user) && record.author == user) || record.client.has_director?(user)
  end
  
  def destroy?
    user.try(:admin?) || record.client.has_manager?(user) || record.client.has_director?(user)
  end
end
