class InvoicePolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end
  
  def show?
    user.try(:admin?) || user.try(:finance?) || record.estimate.job.client.has_belonger?(user)
  end
  
  def create?
    user.try(:admin?) || user.try(:finance?)
  end
  
  def update?
    user.try(:admin?) || user.try(:finance?)
  end
  
  def destroy?
    user.try(:admin?) || user.try(:finance?)
  end
end