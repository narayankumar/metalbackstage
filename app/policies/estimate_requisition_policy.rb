class EstimateRequisitionPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end
  
  def show?
    user.try(:admin?) || user.try(:finance?) || record.job.client.has_handler?(user) || record.job.client.has_director?(user) || record.job.client.has_manager?(user)
  end
  
  def create?
    user.try(:admin?) || record.job.client.has_handler?(user) || record.job.client.has_manager?(user)
  end
  
  def update?
    user.try(:admin?)
  end
  
  def destroy?
    user.try(:admin?) || user.try(:finance?)
  end
end
