class CommentPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end
  
  def show?
    user.try(:admin?) || record.job.client.has_belonger?(user)
  end
  
  def create?
    user.try(:admin?) || record.job.client.has_belonger?(user)
  end
end