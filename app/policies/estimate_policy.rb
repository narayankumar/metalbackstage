class EstimatePolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end
  
  def index?
    user.try(:admin?) || user.try.(:finance?) || record.job.client.has_director?(user) 
  end
  
  def show?
    user.try(:admin?) || user.try(:finance?) || record.job.client.has_handler?(user) || record.job.client.has_director?(user) || record.job.client.has_manager?(user)
  end
  
  def create?
    user.try(:admin?) || user.try(:finance?)
  end
  
  def update?
    user.try(:admin?) || user.try(:finance?)
  end
  
  def destroy?
    user.try(:admin?) || user.try(:finance?)
  end
end




