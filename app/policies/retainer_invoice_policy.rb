class RetainerInvoicePolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end
  
  def show?
    user.try(:admin?) || user.try(:finance?) || record.client.has_manager?(user) || record.client.has_director?(user)
  end
  
  def create?
    user.try(:admin?) || user.try(:finance?)
  end
  
  def update?
    user.try(:admin?) || user.try(:finance?)
  end
  
  def destroy?
    user.try(:admin?) || user.try(:finance?)
  end
end