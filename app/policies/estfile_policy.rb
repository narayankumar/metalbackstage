class EstfilePolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end
  def show?
    user.try(:admin?) || record.estimate.job.client.has_belonger?(user)
  end
  
  def destroy?
    user.try(:admin?) || user.try(:finance?)
  end
end
