class AttachmentPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end
  
  def show?
    user.try(:admin?) || record.job.client.has_belonger?(user)
  end
  
  def destroy?
    user.try(:admin?) || (record.job.client.has_handler?(user) && record.job.author == user) || record.job.client.has_manager?(user)
  end
end
