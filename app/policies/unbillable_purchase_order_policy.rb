class UnbillablePurchaseOrderPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end
  
  def show?
    user.try(:admin?) || user.try(:finance?) || record.client.has_handler?(user) || record.client.has_director?(user) || record.client.has_manager?(user)
  end
  
  def create?
    user.try(:admin?) || record.client.has_manager?(user) || record.client.has_handler?(user)
  end
  
  def update?
    user.try(:admin?) || record.client.has_director?(user)
  end
  
  def destroy?
    user.try(:admin?) || user.try(:finance?)
  end
end
