class InvoicesController < ApplicationController
  before_action :set_estimate
  before_action :set_client_job, only: [:create, :show, :destroy]
  before_action :set_invoice, only: [:show, :edit, :update, :destroy]
  
  def new
    @invoice = @estimate.invoices.build
    authorize @invoice, :create?
  end
  
  def create
    @invoice = @estimate.invoices.build(invoice_params)
    @invoice.author = current_user
    authorize @invoice, :create?
    
    
    if @invoice.save
      flash[:notice] = "Invoice has been created"
      redirect_to [@estimate, @invoice]
    else
      flash[:alert] = "Invoice was not created"
      render "new"
    end
  end
  
  def show
    authorize @invoice, :show?
    
    respond_to do |format|
      format.html
      format.pdf do
        pdf = InvoicePdf.new(@invoice, view_context)
        send_data pdf.render, filename: "invoice_#{@invoice.created_at.strftime("%d/%m/%Y")}.pdf", type: "application/pdf"
      end
    end
  end
  
  def edit
    authorize @invoice, :update?
  end
  
  def update
    authorize @invoice, :update?
    
    if @invoice.update(invoice_params)
      flash[:notice] = "Invoice was updated"
      redirect_to [@estimate, @invoice]
    else
      flash.now[:alert] = "Invoice was not updated"
      render "edit"
    end
  end
  
  def destroy
    authorize @invoice, :destroy?
    
    @invoice.destroy
    flash[:notice] = "Invoice was deleted"
    redirect_to [@client, @job, @estimate]
  end
  
  private
  
  def set_estimate
    @estimate = Estimate.find(params[:estimate_id])
  end
  
  def set_client_job
    jobid = @estimate.job_id
    @job = Job.find(jobid)
    clientid = @job.client_id
    @client = Client.find(clientid)
  end
  
  def set_invoice
    @invoice = @estimate.invoices.find(params[:id])
  end
  
  def invoice_params
    params.require(:invoice).permit(:title, :details, :client, :billable_entity_id, :invoice_type, :amount, :service_tax, :swachh_bharat, :krishi_kalyan_cess, :total_amount, :amount_in_words, :notes, :signoff, :signatory, :client_signatory, :paid_status, :printer_date, :printer_title)
  end
end


