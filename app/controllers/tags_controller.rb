class TagsController < ApplicationController
  def remove
    @job = Job.find(params[:job_id])
    @tag = Tag.find(params[:id])
    authorize @job, :update?
    
    @job.tags.destroy(@tag)
    head :ok
  end
end
