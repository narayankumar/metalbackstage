class ImagesController < ApplicationController
  skip_after_action :verify_authorized
  
  def new
    @index = params[:index].to_i
    @job = Job.new
    @job.images.build
    render layout: false
  end
  
  def show
    image = Image.find(params[:id])
    authorize image, :show?
    send_file  file_to_send(image), disposition: :inline
  end
  
  def destroy    
    @image= Image.find(params[:id])
    @job = @image.job
    @client = @job.client
    authorize @image, :destroy?
    
    @image.destroy
    flash[:notice] = "Image was deleted"
    redirect_to [@client, @job]
  end
  
  private
  
  def file_to_send(image)
    if URI.parse(image.image.url).scheme
      filename = "/tmp/#{image.attributes["image"]}"
      File.open(filename, "wb+") do |tf|
        tf.write open(image.image.url).read
      end
      filename
    else
      image.image.path
    end
  end
end
