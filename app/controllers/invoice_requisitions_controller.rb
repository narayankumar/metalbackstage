class InvoiceRequisitionsController < ApplicationController
  before_action :set_estimate
  before_action :set_invoice_requisition, only: [:show, :edit, :update, :destroy]
  before_action :set_client_job, only: [:create, :show, :destroy]
  
  def new
    @invoice_requisition = @estimate.invoice_requisitions.build
    authorize @invoice_requisition, :create?
  end
  
  def create
    @creator = InvoiceRequisitionCreator.build(@estimate.invoice_requisitions, current_user, invoice_requisition_params)
    authorize @creator.invoice_requisition, :create?
    
    if @creator.save
      flash[:notice] = "Invoice Requisition has been created"
      @invoice_requisition = @creator.invoice_requisition
      @estimate = @invoice_requisition.estimate
      redirect_to [@estimate, @invoice_requisition]
    else
      flash.now[:alert] = "Invoice Requisition was not created"
      @invoice_requisition = @creator.invoice_requisition
      render "new"
    end
  end
  
  def show
    authorize @invoice_requisition, :show?
  end
  
  def edit
    authorize @invoice_requisition, :update?
  end
  
  def update
    authorize @invoice_requisition, :update?
    
    if @invoice_requisition.update(invoice_requisition_params)
      flash[:notice] = "Invoice requisition was updated"
      redirect_to [@estimate, @invoice_requisition]
    else
      flash.now[:alert] = "Invoice requisition was not updated"
      render "edit"
    end
  end
  
  def destroy
    authorize @invoice_requisition, :destroy?
    
    @invoice_requisition.destroy
    flash[:notice] = "Invoice requisition was deleted"
    redirect_to [@client, @job, @estimate]
  end
  
  private
  
  def set_estimate
    @estimate = Estimate.find(params[:estimate_id])
  end
  
  def set_invoice_requisition
    @invoice_requisition = @estimate.invoice_requisitions.find(params[:id])
  end
  
  def set_client_job
    jobid = @estimate.job_id
    @job = Job.find(jobid)
    clientid = @job.client_id
    @client = Client.find(clientid)
  end
  
  def invoice_requisition_params
    params.require(:invoice_requisition).permit(:notes, :approval)
  end
end
