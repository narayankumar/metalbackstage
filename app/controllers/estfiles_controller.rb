class EstfilesController < ApplicationController
  skip_after_action :verify_authorized, only: [:new, :edit]
  
  def show
    estfile = Estfile.find(params[:id])
    authorize estfile, :show?
    send_file file_to_send(estfile), disposition: :inline
  end
  
  def new
    @index = params[:index].to_i
    @estimate = Estimate.new
    @estimate.estfiles.build
    render layout: false
  end
  
  def edit
    estfile = Estfile.find(params[:id])
  end
  
  private
  
  def file_to_send(estfile)
    if URI.parse(estfile.file.url).scheme
      filename = "/tmp/#{estfile.attributes["file"]}"
      File.open(filename, "wb+") do |tf|
        tf.write open(estfile.file.url).read
      end
      filename
    else
      estfile.file.path
    end
  end
end
