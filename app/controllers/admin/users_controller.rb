class Admin::UsersController < Admin::ApplicationController
  before_action :set_clients, only: [:new, :create, :edit, :update]
  before_action :set_user, only: [:show, :edit, :update, :archive]
  
  def index
    @users = User.excluding_archived.order(:email)
  end
  
  def show
  end
  
  def new
    @user = User.new
  end
  
  def create
    @user = User.new(user_params)
    build_roles_for(@user)
    
    if @user.save
      flash[:notice] = "Metalloid has been created"
      redirect_to admin_users_path
    else
      flash.now[:alert] = "Metalloid has not been created"
      render "new"
    end
  end
  
  def edit
  end
  
  def update
    if params[:user][:password].blank?
      params[:user].delete(:password)
    end
    
    User.transaction do
      @user.roles.clear
      erase_empty_rows
      build_roles_for(@user)
    
      if @user.update(user_params)
        flash[:notice] = "Metalloid has been updated"
        redirect_to admin_users_path
      else
        flash.now[:alert] = "Metalloid was not updated"
        render "edit"
        raise ActiveRecord::Rollback
      end
    end
  end
  
  def archive
    if @user == current_user
      flash[:alert] = "You cannot archive yourself!"
    else
      @user.archive    
      flash[:notice] = "Metalloid has been archived"
    end
      
      redirect_to admin_users_path
  end

  
  private
  
  def user_params
    params.require(:user).permit(:email, :password, :admin, :finance, :casual, :sick, :privileged)
  end
  
  def set_user
    @user = User.find(params[:id])
  end
  
  def set_clients
    @clients = Client.order(:name)
  end
  
  def erase_empty_rows
    rows = Role.where(user_id: nil)
    rows.each do |row|
      row.destroy
    end
  end
  
  def build_roles_for(user)
    role_data = params.fetch(:roles, [])
    role_data.each do |client_id, role_name|
      if role_name.present?
        @user.roles.build(client_id: client_id, role: role_name)
      end
    end
  end
  
end
