class Admin::VentypesController < Admin::ApplicationController
  
  before_action :set_ventype, only: [:show, :edit, :update, :destroy]
  
  def index
    @ventypes = Ventype.all
  end
  
  def new
    @ventype = Ventype.new
  end
  
  def create
    @ventype = Ventype.new(ventype_params)
    
    if @ventype.save
      flash[:notice] = "Ventype has been created"
      redirect_to admin_ventype_path(@ventype)
    else
      flash.now[:alert] = "Ventype was not created"
      render "new"
    end
  end
  
  def show  
  end
  
  def edit
  end
  
  def update    
    if @ventype.update(ventype_params)
      flash[:notice] = "Ventype was updated"
      redirect_to admin_ventype_path(@ventype)
    else
      flash.now[:alert] = "Ventype was not updated"
      render "edit"
    end
  end
  
  def destroy 
    @ventype.destroy 
    flash[:notice] = "Ventype was deleted"
    redirect_to admin_ventypes_path
  end
  
  private
  
  def ventype_params
    params.require(:ventype).permit(:name)
  end
  
  def set_ventype
    @ventype = Ventype.find(params[:id])
  end
end



