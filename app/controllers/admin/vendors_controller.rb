class Admin::VendorsController < Admin::ApplicationController
  before_action :set_ventype
  before_action :set_vendor, only: [:show, :edit, :update, :destroy]
  
  def new
    @vendor = @ventype.vendors.build
  end
  
  def create
    @vendor = @ventype.vendors.build(vendor_params)
    
    if @vendor.save
      flash[:notice] = "Vendor has been created"
      redirect_to [:admin, @ventype, @vendor]
    else
      flash.now[:alert] = "Vendor was not created"
      render "new"
    end
  end
  
  def show
  end
  
  def edit
  end
  
  def update
    
    if @vendor.update(vendor_params)
      flash[:notice] = "Vendor was updated"
      redirect_to [:admin, @ventype, @vendor]
    else
      flash.now[:alert] = "Vendor was not updated"
      render "edit"
    end
  end
  
  def destroy
    @vendor.destroy    
    flash[:notice] = "Vendor was deleted"
    
    redirect_to [:admin, @ventype]
  end
  
  private
  
  def vendor_params
    params.require(:vendor).permit(:name, :contact_name, :phone, :email, :address, :bank, :bank_acct, :ifsc, :service_tax, :pan, :tan)
  end
  
  def set_ventype
    @ventype = Ventype.find(params[:ventype_id])
  end
  
  def set_vendor
    @vendor = @ventype.vendors.find(params[:id])
  end
  
end
