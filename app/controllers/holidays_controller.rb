class HolidaysController < ApplicationController
  skip_after_action :verify_authorized, :verify_policy_scoped
  
  def new
    @holiday = Holiday.new
  end
  
  def create
    @holiday = Holiday.new(holiday_params)
    
    if @holiday.save
      flash[:notice] = "Holiday has been created"
      redirect_to "/"
    else
      flash.now[:alert] = "Holiday was not created"
      render "new"
    end
  end
  
  def edit
    @holiday = Holiday.find(params[:id])
  end
  
  def update
    @holiday = Holiday.find(params[:id])
    
    if @holiday.update(holiday_params)
      flash[:notice] = "Holiday was updated"
      redirect_to "/"
    else
      flash.now[:alert] = "Holiday was not updated"
      render "edit"
    end
  end
  
  def destroy
    @holiday = Holiday.find(params[:id])
    @holiday.destroy
    
    flash[:notice] = "Holiday was deleted"
    redirect_to "/"
  end
  
  private
  
  def holiday_params
    params.require(:holiday).permit(:name, :whichday, :mumbai, :delhi)
  end
end

