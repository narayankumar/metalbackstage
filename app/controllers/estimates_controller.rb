class EstimatesController < ApplicationController
  before_action :set_client_job
  before_action :set_estimate, only: [:show, :edit, :update, :destroy]
  
  def new
    @estimate = @job.estimates.build
    authorize @estimate, :create?
    @estimate.estfiles.build
  end
  
  def create
    @estimate = @job.estimates.build(estimate_params)
    @estimate.author = current_user
    authorize @estimate, :create?
    
    if @estimate.save
      flash[:notice] = "Estimate has been created"
      redirect_to [@client, @job, @estimate]
    else
      flash.now[:alert] = "Estimate was not created"
      render "new"
    end
  end
  
  def show  
    vendor = @estimate.vendor
    authorize @estimate, :show?
    
    respond_to do |format|
      format.html
      format.pdf do
        pdf = EstimatePdf.new(@estimate, view_context)
        send_data pdf.render, filename: "estimate_#{@estimate.created_at.strftime("%d/%m/%Y")}.pdf", type: "application/pdf"
      end
    end
  end
  
  def edit
    authorize @estimate, :update?
    @estimate.estfiles.build
  end
  
  def update
    authorize @estimate, :update?
    
    if @estimate.update(estimate_params)
      flash[:notice] = "Estimate was updated"
      redirect_to [@client, @job, @estimate]
    else
      flash.now[:alert] = "Estimate was not updated"
      render "edit"
    end
  end
  
  def destroy
    authorize @estimate, :destroy?
    
    @estimate.destroy
    flash[:notice] = "Estimate was deleted"
    redirect_to [@client, @job]
  end
  
  private
  
  def estimate_params
    params.require(:estimate).permit(:name, :description, :job_id, :client_id, :billable_entity_id, :vendor_id, :printer_date, :printer_title, :amount, :estimate_type, :notes, :signoff, :signatory, :status, :client_signatory, estfiles_attributes: [:file, :file_cache])
  end
  
  def set_client_job
    @client = Client.find(params[:client_id])
    @job = Job.find(params[:job_id])
  end
  
  def set_estimate
    @estimate = @job.estimates.find(params[:id])
  end
end