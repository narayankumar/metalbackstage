class UnbillablePurchaseOrdersController < ApplicationController
  before_action :set_client
  before_action :set_unbillable_purchase_order, only: [:show, :edit, :update, :destroy]
  
  def new
    @unbillable_purchase_order = @client.unbillable_purchase_orders.build
    authorize @unbillable_purchase_order, :create?
  end
  
  def create
    @creator = UnbillablePurchaseOrderCreator.build(@client.unbillable_purchase_orders, current_user, unbillable_purchase_order_params)
    authorize @creator.unbillable_purchase_order, :create?
    
    if @creator.save
      flash[:notice] = "Unbillable purchase order has been created"
      @unbillable_purchase_order = @creator.unbillable_purchase_order
      @client = @unbillable_purchase_order.client
      redirect_to [@client, @unbillable_purchase_order]
    else
      flash.now[:alert] = "Unbillable purchase order was not created"
      @unbillable_purchase_order = @creator.unbillable_purchase_order
      render "new"
    end
  end
  
  def show
    authorize @unbillable_purchase_order, :show?
    
    respond_to do |format|
      format.html
      format.pdf do
        pdf = UnbillablePurchaseOrderPdf.new(@unbillable_purchase_order, view_context)
        send_data pdf.render, filename: "unbillable_purchase_order_#{@unbillable_purchase_order.created_at.strftime("%d/%m/%Y")}.pdf", type: "application/pdf"
      end
    end
  end
  
  def edit
    authorize @unbillable_purchase_order, :update?
  end
  
  def update
    authorize @unbillable_purchase_order, :update?
    
    if @unbillable_purchase_order.update(unbillable_purchase_order_params)
      flash[:notice] = "Unbillable purchase order was updated"
      redirect_to [@client, @unbillable_purchase_order]
    else
      flash.now[:alert] = "Unbillable purchase order was not updated"
      render "edit"
    end
  end
  
  def destroy
    authorize @unbillable_purchase_order, :destroy?
    
    @unbillable_purchase_order.destroy
    flash[:notice] = "Unbillable purchase order was deleted"
    
    redirect_to @client
  end
  
  private
  
  def set_client
    @client = Client.find(params[:client_id])
  end
  
  def set_unbillable_purchase_order
    @unbillable_purchase_order = @client.unbillable_purchase_orders.find(params[:id])
  end
  
  def unbillable_purchase_order_params
    params.require(:unbillable_purchase_order).permit(:details, :amount, :vendor_id, :bill_received_status, :bill_paid_status, :approval_status)
  end
end
