class CommentsController < ApplicationController
  before_action :set_job
  
  def create    
    @creator = CommentCreator.build(@job.comments, current_user, comment_params)
    authorize @creator.comment, :create?
    
    if @creator.save
      flash[:notice] = "Comment has been created"
      redirect_to [@job.client, @job]
    else
      flash[:alert] = "Comment can't be blank. Comment was not created"
      @client = @job.client
      @comment = @creator.comment
      redirect_to [@job.client, @job]
    end
  end
  
  private
  
  def set_job
    @job = Job.find(params[:job_id])
  end
  
  def comment_params
    params.require(:comment).permit(:text, :status_id, :tag_names)
  end
end
