class JobsController < ApplicationController
  before_action :set_client
  before_action :set_job, only: [:show, :edit, :update, :destroy, :watch]
  
  def new
    @job = @client.jobs.build
    authorize @job, :create?
    @job.attachments.build
    @job.images.build
    @job.ideas.build
  end
  
  def create
    @job = @client.jobs.build(job_params)
    @job.author = current_user
    authorize @job, :create?
    
    if @job.save
      flash[:notice] = "Job has been created"
      redirect_to [@client, @job]
    else
      flash.now[:alert] = "Job was not created"
      render "new"
    end
  end
  
  def show
    authorize @job, :show?
    @comment = @job.comments.build(status_id: @job.status_id)
  end
  
  def edit
    authorize @job, :update?
    @job.attachments.build
    @job.images.build
    @job.ideas.build
  end
  
  def update
    authorize @job, :update?
    
    if @job.update(job_params)
      flash[:notice] = "Job was updated"
      redirect_to [@client, @job]
    else
      flash.now[:alert] = "Job was not updated"
      render "edit"
    end
  end
  
  def destroy
    authorize @job, :destroy?
    @job.destroy
    flash[:notice] = "Job was deleted"
    redirect_to @client 
  end
  
  def search
    authorize @client, :show?
    if params[:search].present?
      @jobs = @client.jobs.order(created_at: :desc).search(params[:search]).paginate(:page => params[:page], :per_page => 25)
    else
      @jobs = @client.jobs.order(created_at: :desc)
    end
    render "clients/show"
  end  
  
  
  def watch
    authorize @job, :show?
    if @job.watchers.exists?(current_user.id)
      @job.watchers.destroy(current_user)
      flash[:notice] = "You are no longer watching this job"
    else
      @job.watchers << current_user
      flash[:notice] = "You are now watching this job"
    end
    
    redirect_to client_job_path(@job.client, @job)
  end
  
  private
  
  def set_client
    @client = Client.find(params[:client_id])
  end
  
  def set_job
    @job = @client.jobs.find(params[:id])
    @images = @job.images
  end
  
  def job_params
    params.require(:job).permit(:name, :description, :tag_names, :brief_date, :deadline, attachments_attributes: [:file, :file_cache], images_attributes: [:image, :image_cache], ideas_attributes: [:idea, :idea_cache])
  end
end
