class Finance::ApplicationController < ApplicationController
  skip_after_action :verify_authorized, :verify_policy_scoped
  before_action :authorize_finance!
  
  def index
  end
  
  private
  
  def authorize_finance!
    authenticate_user!
    
    unless current_user.finance?
      redirect_to root_path, alert: "You must be in finance to do that"
    end
  end
end
