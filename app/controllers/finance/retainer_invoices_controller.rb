class Finance::RetainerInvoicesController < Finance::ApplicationController
  def index
    @retainer_invoices = RetainerInvoice.paginate(:page => params[:page], :per_page => 25).order("created_at desc")
  end
end
