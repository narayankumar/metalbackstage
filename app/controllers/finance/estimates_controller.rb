class Finance::EstimatesController < Finance::ApplicationController
  
  def index
    @estimates = Estimate.paginate(:page => params[:page], :per_page => 25).order("created_at desc")
  end
end
