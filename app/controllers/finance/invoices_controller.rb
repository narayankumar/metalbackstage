class Finance::InvoicesController < Finance::ApplicationController
  def index
    @invoices = Invoice.paginate(:page => params[:page], :per_page => 25).order("created_at desc")
  end
end
