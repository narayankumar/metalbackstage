class Finance::PurchaseOrdersController < Finance::ApplicationController
  def index
    @purchase_orders = PurchaseOrder.paginate(:page => params[:page], :per_page => 25).order("created_at desc")
  end
end
