class Finance::UnbillablePurchaseOrdersController < Finance::ApplicationController
  def index
    @unbillable_purchase_orders = UnbillablePurchaseOrder.where(approval_status: "Approved").paginate(:page => params[:page], :per_page => 25).order("created_at desc")
  end
end
