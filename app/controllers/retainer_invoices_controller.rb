class RetainerInvoicesController < ApplicationController
  before_action :set_client
  before_action :set_retainer_invoice, only: [:show, :edit, :update, :destroy]
  
  def new
    @retainer_invoice = @client.retainer_invoices.build
    authorize @retainer_invoice, :create?
  end
  
  def create
    @retainer_invoice = @client.retainer_invoices.build(retainer_invoice_params)
    @retainer_invoice.author = current_user
    authorize @retainer_invoice, :create?
    
    if @retainer_invoice.save
      flash[:notice] = "Retainer invoice has been created"
      redirect_to [@client, @retainer_invoice]
    else
      flash.now[:alert] = "Retainer invoice was not created"
      render "new"
    end
  end
  
  def show
    authorize @retainer_invoice, :show?
    
    respond_to do |format|
      format.html
      format.pdf do
        pdf = RetainerInvoicePdf.new(@retainer_invoice, view_context)
        send_data pdf.render, filename: "retainer_invoice_#{@retainer_invoice.created_at.strftime("%d/%m/%Y")}.pdf", type: "application/pdf"
      end
    end
  end
  
  def edit
    authorize @retainer_invoice, :update?
  end
  
  def update
    authorize @retainer_invoice, :update?
    
    if @retainer_invoice.update(retainer_invoice_params)
      flash[:notice] = "Retainer invoice was updated"
      redirect_to [@client, @retainer_invoice]
    else
      flash.now[:alert] = "Retainer invoice was not updated"
      render "edit"
    end
  end
  
  def destroy
    authorize @retainer_invoice, :destroy?
    
    @retainer_invoice.destroy
    flash[:notice] = "Retainer invoice was deleted"
    
    redirect_to @client
  end
  
  private
  
  def set_client
    @client = Client.find(params[:client_id])
  end
  
  def retainer_invoice_params
    params.require(:retainer_invoice).permit(:title, :narration, :amount, :service_tax, :swachh_bharat, :krishi_kalyan_cess, :total_amount, :amount_in_words, :notes, :signoff, :signatory, :status, :printer_title, :printer_date, :billable_entity_id)
  end
  
  def set_retainer_invoice
    @retainer_invoice = @client.retainer_invoices.find(params[:id])
  end
end
