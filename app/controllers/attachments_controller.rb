class AttachmentsController < ApplicationController
  skip_after_action :verify_authorized, only: [:new, :edit]
  
  def show
    attachment = Attachment.find(params[:id])
    authorize attachment, :show?
    send_file file_to_send(attachment), disposition: :inline
  end
  
  def new
    @index = params[:index].to_i
    @job = Job.new
    @job.attachments.build
    render layout: false
  end
  
  def edit
    attachment = Attachment.find(params[:id])
  end
  
  def destroy    
    @attachment= Attachment.find(params[:id])
    @job = @attachment.job
    @client = @job.client
    authorize @attachment, :destroy?
    
    @attachment.destroy
    flash[:notice] = "Image was deleted"
    redirect_to [@client, @job]
  end
  
  private
  
  def file_to_send(attachment)
    if URI.parse(attachment.file.url).scheme
      filename = "/tmp/#{attachment.attributes["file"]}"
      File.open(filename, "wb+") do |tf|
        tf.write open(attachment.file.url).read
      end
      filename
    else
      attachment.file.path
    end
  end
end
