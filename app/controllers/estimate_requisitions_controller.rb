class EstimateRequisitionsController < ApplicationController
  before_action :set_job
  before_action :set_estimate_requisition, only: [:show, :edit, :update, :destroy]
  before_action :set_client, only: [:show, :new, :create, :destroy]
  
  def new
    @estimate_requisition = @job.estimate_requisitions.build
    authorize @estimate_requisition, :create?
  end
  
  def create
    @creator = EstimateRequisitionCreator.build(@job.estimate_requisitions, current_user, estimate_requisition_params)
    authorize @creator.estimate_requisition, :create?
    
    if @creator.save
      flash[:notice] = "Estimate Requisition has been created"
      @estimate_requisition = @creator.estimate_requisition
      @job = @estimate_requisition.job
      redirect_to [@job, @estimate_requisition]
    else
      flash.now[:alert] = "Estimate Requisition was not created"
      @estimate_requisition = @creator.estimate_requisition
      render "new"
    end
  end
  
  def show
    authorize @estimate_requisition, :show?
  end
  
  def edit
    authorize @estimate_requisition, :update?
  end
  
  def update
    authorize @estimate_requisition, :update?
    
    if @estimate_requisition.update(estimate_requisition_params)
      flash[:notice] = "Estimate requisition was updated"
      redirect_to [@job, @estimate_requisition]
    else
      flash.now[:alert] = "Estimate requisition was not updated"
      render "edit"
    end
  end
  
  def destroy
    authorize @estimate_requisition, :destroy?
    
    @estimate_requisition.destroy
    flash[:notice] = "Estimate requisition was deleted"
    redirect_to [@client, @job]
  end
  
  private
  
  def set_job
    @job = Job.find(params[:job_id])
  end
  
  def set_estimate_requisition
    @estimate_requisition = @job.estimate_requisitions.find(params[:id])
  end
  
  def set_client
    clientid = @job.client_id
    @client = Client.find(clientid)
  end
  
  def estimate_requisition_params
    params.require(:estimate_requisition).permit(:details, :amount, :vendor_id, :title, :er_attach, :er_attach_cache)
  end
end
