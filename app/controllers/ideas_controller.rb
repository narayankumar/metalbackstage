class IdeasController < ApplicationController
  skip_after_action :verify_authorized, only: [:new]
  
  def new
    @index = params[:index].to_i
    @job = Job.new
    @job.ideas.build
    render layout: false
  end
  
  def show
    idea = Idea.find(params[:id])
    authorize idea, :show?
    send_file file_to_send(idea), disposition: :inline
  end
  
  def destroy    
    @idea = Idea.find(params[:id])
    @job = @idea.job
    @client = @job.client
    authorize @idea, :destroy?
    
    @idea.destroy
    flash[:notice] = "Idea was deleted"
    redirect_to [@client, @job]
  end
  
  private
  
  def file_to_send(idea)
    if URI.parse(idea.idea.url).scheme
      filename = "/tmp/#{idea.attributes["idea"]}"
      File.open(filename, "wb+") do |tf|
        tf.write open(idea.idea.url).read
      end
      filename
    else
      idea.idea.path
    end
  end
end





  
  