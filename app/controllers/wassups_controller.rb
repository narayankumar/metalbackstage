class WassupsController < ApplicationController
  skip_after_action :verify_authorized, :verify_policy_scoped
  
  def new
    @wassup = Wassup.new
  end
  
  def create
    @wassup = Wassup.new(wassup_params)
    
    if @wassup.save
      flash[:notice] = "Wassup has been created"
      redirect_to "/"
    else
      flash.now[:alert] = "Wassup was not created"
      render "new"
    end
  end
  
  def edit
    @wassup = Wassup.find(params[:id])
  end
  
  def update
    @wassup = Wassup.find(params[:id])
    
    if @wassup.update(wassup_params)
      flash[:notice] = "Wassup was updated"
      redirect_to "/"
    else
      flash.now[:alert] = "Wassup was not updated"
      render "edit"
    end
  end
  
  def destroy
    @wassup = Wassup.find(params[:id])
    @wassup.destroy
    
    flash[:notice] = "Wassup was deleted"
    redirect_to "/"
  end
  
  private
  
  def wassup_params
    params.require(:wassup).permit(:title, :text, :wimage, :wimage_cache, :remove_wimage)
  end
end
