class PurchaseOrdersController < ApplicationController
  before_action :set_estimate
  before_action :set_purchase_order, only: [:show, :edit, :update, :destroy]
  before_action :set_client_job, only: [:create, :show, :destroy]
  
  def new
    @purchase_order = @estimate.purchase_orders.build
    authorize @purchase_order, :create?
  end
  
  def create
    @purchase_order = @estimate.purchase_orders.build(purchase_order_params)
    @purchase_order.author = current_user
    authorize @purchase_order, :create?
    
    if @purchase_order.save
      flash[:notice] = "Purchase order has been created"
      redirect_to [@estimate, @purchase_order]
    else
      flash[:alert] = "Purchase order was not created"
      render "new"
    end
  end
  
  def show
    authorize @purchase_order, :show?
    
    respond_to do |format|
      format.html
      format.pdf do
        pdf = PurchaseOrderPdf.new(@purchase_order, view_context)
        send_data pdf.render, filename: "purchase_order_#{@purchase_order.created_at.strftime("%d/%m/%Y")}.pdf", type: "application/pdf"
      end
    end
  end
  
  def edit
    authorize @purchase_order, :update?
  end
  
  def update
    authorize @purchase_order, :create?
    
    if @purchase_order.update(purchase_order_params)
      flash[:notice] = "Purchase order was updated"
      redirect_to [@estimate, @purchase_order]
    else
      flash.now[:alert] = "Purchase order was not updated"
      render "edit"
    end
  end
  
  def destroy
    authorize @purchase_order, :destroy?
    
    @purchase_order.destroy
    flash[:notice] = "Purchase order was deleted"
    redirect_to [@client, @job, @estimate]
  end
  
  private
  
  def purchase_order_params
    params.require(:purchase_order).permit(:title, :amount, :amount_in_words, :details, :payment_terms, :payment_details, :bill_given_status, :client_paid_status, :vendor_paid_status, :vendor_id)  
  end
  
  def set_estimate
    @estimate = Estimate.find(params[:estimate_id])
  end
  
  def set_purchase_order
    @purchase_order = @estimate.purchase_orders.find(params[:id])
  end
  
  def set_client_job
    jobid = @estimate.job_id
    @job = Job.find(jobid)
    clientid = @job.client_id
    @client = Client.find(clientid)
  end
end
