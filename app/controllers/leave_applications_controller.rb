class LeaveApplicationsController < ApplicationController
  skip_after_action :verify_authorized, :verify_policy_scoped
  
  def new
    @leave_application = LeaveApplication.new
  end
  
  def create
    @leave_application = LeaveApplication.new(leave_application_params)
    @leave_application.author = current_user
    
    if @leave_application.save
      flash[:notice] = "Your leave application is now under process"
      redirect_to "/"
      notify_directors
    else
      flash.now[:alert] = "Your leave application was not created"
      render "new"
    end
  end
  
  def show
    @leave_application = LeaveApplication.find(params[:id])
  end
  
  def edit
    @leave_application = LeaveApplication.find(params[:id])
  end
  
  def update
    @leave_application = LeaveApplication.find(params[:id])
    
    if @leave_application.update(leave_application_params)
      flash[:notice] = "Leave Application was updated"
      redirect_to "/"
    else
      flash.now[:alert] = "Leave Application was not updated"
      render "edit"
    end
  end
  
  def destroy
    @leave_application = LeaveApplication.find(params[:id])
    @leave_application.destroy
    
    flash[:notice] = "Leave Application was deleted"
    redirect_to "/"
  end
  
  private
  
  def leave_application_params
    params.require(:leave_application).permit(:details, :from_date, :to_date, :leave_type, :status, :author_id)
  end
  
  def notify_directors
    client = Client.find(2)
    if !(client.roles.find_by(role: "director")).nil?
      (client.roles.where(role: "director")).each do |role|
        user = User.find(role.user_id)
        LeaveApplicationNotifier.created(@leave_application, user).deliver_now
      end
    end
    
    user = User.find(9)
    LeaveApplicationNotifier.created(@leave_application, user).deliver_now
  end
  
end
