class BillableEntitiesController < ApplicationController
  before_action :set_client
  before_action :set_billable_entity, only: [:show, :edit, :update, :destroy]
  
  def new
    @billable_entity = @client.billable_entities.build
    authorize @billable_entity, :create?
  end
  
  def create
    @billable_entity = @client.billable_entities.build(billable_entity_params)
    authorize @billable_entity, :create?
    
    if @billable_entity.save
      flash[:notice] = "Billable entity has been created"
      redirect_to [@client, @billable_entity]
    else
      flash.now[:alert] = "Billable entity was not created"
      render "new"
    end
  end
  
  def show
    authorize @billable_entity, :show?
  end
  
  def edit
    authorize @billable_entity, :update?
  end
  
  def update
    authorize @billable_entity, :update?
    
    if @billable_entity.update(billable_entity_params)
      flash[:notice] = "Billable entity was updated"
      redirect_to [@client, @billable_entity]
    else
      flash.now[:alert] = "Billable entity was not updated"
      render "edit"
    end
  end
  
  def destroy
    authorize @billable_entity, :destroy?
    
    @billable_entity.destroy
    flash[:notice] = "Billable entity was deleted"
    
    redirect_to @client
  end
  
  private
  
  def set_client
    @client = Client.find(params[:client_id])
  end
  
  def set_billable_entity
    @billable_entity = @client.billable_entities.find(params[:id])
  end
  
  def billable_entity_params
    params.require(:billable_entity).permit(:name, :phone, :contact_name, :email, :address)
  end
end
