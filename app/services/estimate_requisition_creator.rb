class EstimateRequisitionCreator
  attr_reader :estimate_requisition
  
  def self.build(scope, current_user, estimate_requisition_params)
    estimate_requisition = scope.build(estimate_requisition_params)
    estimate_requisition.author = current_user
    new(estimate_requisition)
  end
  
  def initialize(estimate_requisition)
    @estimate_requisition = estimate_requisition
  end
  
  def save
    if @estimate_requisition.save
      notify_finance
    end
  end
  
  def notify_finance
    (User.where(finance: true)).each do |user|
      EstimateRequisitionNotifier.created(@estimate_requisition, user).deliver_now
    end
  end
end

