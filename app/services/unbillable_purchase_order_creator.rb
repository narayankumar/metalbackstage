class UnbillablePurchaseOrderCreator
  attr_reader :unbillable_purchase_order
  
  def self.build(scope, current_user, unbillable_purchase_order_params)
    unbillable_purchase_order = scope.build(unbillable_purchase_order_params)
    unbillable_purchase_order.author = current_user
    new(unbillable_purchase_order)
  end
  
  def initialize(unbillable_purchase_order)
    @unbillable_purchase_order = unbillable_purchase_order
  end
  
  def save
    if @unbillable_purchase_order.save
      notify_directors
    end
  end
  
  def notify_directors
    (User.where(finance: true)).each do |user|
      UnbillablePurchaseOrderNotifier.created(@unbillable_purchase_order, user).deliver_now
    end
  end
end

