class InvoiceRequisitionCreator
  attr_reader :invoice_requisition
  
  def self.build(scope, current_user, invoice_requisition_params)
    invoice_requisition = scope.build(invoice_requisition_params)
    invoice_requisition.author = current_user
    new(invoice_requisition)
  end
  
  def initialize(invoice_requisition)
    @invoice_requisition = invoice_requisition
  end
  
  def save
    if @invoice_requisition.save
      notify_finance
    end
  end
  
  def notify_finance
    (User.where(finance: true)).each do |user|
      InvoiceRequisitionNotifier.created(@invoice_requisition, user).deliver_now
    end
  end
end