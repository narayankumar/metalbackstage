require 'csv'

namespace :import do
  desc "Import jobs from csv"
  task jobs: :environment do
    filename = File.join Rails.root, "axismutualjobs.csv"
    counter = 0
    
    CSV.foreach(filename, headers: true) do |row|
      job = Job.create(name: row["name"], description: row["description"], client_id: row["client_id"], brief_date: row["brief_date"], deadline: row["deadline"], serial_number: row["serial_number"], author_id: row["author_id"])
      puts "#{job.errors.full_messages.join(",")}" if job.errors.any?
      counter += 1 if job.persisted?
    end
    
    puts "Imported #{counter} jobs"
  end
end