Rails.application.routes.draw do

  namespace :finance do
    root 'application#index'
    resources :estimates, only: [:index]
    resources :invoices, only: [:index]
    resources :retainer_invoices, only: [:index]
    resources :purchase_orders, only: [:index]
    resources :unbillable_purchase_orders, only: [:index]
  end

  namespace :admin do
    root 'application#index'
    resources :clients, only: [:new, :create, :destroy]
    resources :users do
      member do
        patch :archive
      end
    end
    resources :ventypes do
      resources :vendors
    end
  end

  devise_for :users
  root 'clients#index'
  
  resources :clients, only: [:index, :show, :edit, :update] do
    resources :billable_entities
    resources :unbillable_purchase_orders, except: [:index]
    resources :retainer_invoices, except: [:index]
    resources :jobs do
      collection do
        get :search
      end
      member do
        post :watch
      end
      resources :estimates, except: [:index]
    end
  end
  
  resources :estimates, only: [] do
    resources :invoices, except: [:index]
    resources :invoice_requisitions
    resources :purchase_orders, except: [:index]
  end
  
  resources :jobs, only: [] do
    resources :comments, only: [:create]
    resources :estimate_requisitions
    resources :tags, only: [] do
      member do
        delete :remove
      end
    end
  end
  
  resources :wassups
  resources :holidays
  resources :leave_applications
  
  
  resources :attachments, only: [:show, :new, :destroy]
  resources :images, only: [:show, :new, :destroy]
  resources :ideas, only: [:show, :new, :destroy]
  resources :estfiles, only: [:show, :new, :destroy]
end
