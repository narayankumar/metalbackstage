# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
unless Status.exists?
  Status.create(name: "Concept", color: "#800000")
  Status.create(name: "Production", color: "#808000")
  Status.create(name: "Estimate/Invoice", color: "#9932CC")
  Status.create(name: "Delivered", color: "#2F4F4F")
  Status.create(name: "Scrapped", color: "#FFA500")
  Status.create(name: "Re-production", color: "#4682B4")
end
  
unless User.exists?(email: "admin@metalcomm.com")
  User.create!(email: "admin@metalcomm.com", password: "password", admin: true)
end

unless User.exists?(email: "metalloid@metalcomm.com")
  User.create!(email: "metalloid@metalcomm.com", password: "password")
end

["PGI", "Medimix"].each do |name|
  unless Client.exists?(name: name)
    Client.create!(name: name, description: "A sample client about #{name}")
  end
end

Ventype.create!([{
  name: "Art Freelance"
  },
  
  {
    name: "Beta Tape Supplier"
  },
  
  {
    name: "Copywriter"
  },
  
  {
    name: "Corporate Gifting"
  },
  
  {
  name: "Digital Printing"
  },
  
  {
    name: "Dummy Model Maker"
  },
  
  {
    name: "Edit Studio"
  },
  
  {
    name: "Event Management"
  },
  
  {
    name: "Film Production"
  },
  
  {
  name: "Illustration"
  },
  
  {
    name: "Image Library"
  },
  
  {
    name: "Image Retouching"
  },
  
  {
    name: "Language Translation"
  },
  
  {
    name: "Marketing Research"
  },
  
  {
    name: "Online Services"
  },
  
  {
  name: "Photography"
  },
  
  {
    name: "Printing-Processing"
  },
  
  {
    name: "Proof Checking"
  },
  
  {
    name: "Radio Production"
  },
  
  {
    name: "Studio Freelance"
  },
  
  {
  name: "TVC Library"
  }])

Vendor.create!([{
  name: "I N T E R N A L",
  ventype_id: "1"
  }])