# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160629072838) do

  create_table "attachments", force: :cascade do |t|
    t.string   "file"
    t.integer  "job_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "attachments", ["job_id"], name: "index_attachments_on_job_id"

  create_table "billable_entities", force: :cascade do |t|
    t.string   "name"
    t.string   "phone"
    t.integer  "client_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "contact_name"
    t.string   "email"
    t.text     "address"
  end

  add_index "billable_entities", ["client_id"], name: "index_billable_entities_on_client_id"

  create_table "clients", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "comments", force: :cascade do |t|
    t.text     "text"
    t.integer  "job_id"
    t.integer  "author_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "status_id"
    t.integer  "previous_status_id"
  end

  add_index "comments", ["author_id"], name: "index_comments_on_author_id"
  add_index "comments", ["job_id"], name: "index_comments_on_job_id"
  add_index "comments", ["previous_status_id"], name: "index_comments_on_previous_status_id"

  create_table "estfiles", force: :cascade do |t|
    t.string   "file"
    t.integer  "estimate_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "estfiles", ["estimate_id"], name: "index_estfiles_on_estimate_id"

  create_table "estimate_requisitions", force: :cascade do |t|
    t.text     "details"
    t.integer  "amount"
    t.integer  "job_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "title"
    t.integer  "vendor_id"
    t.integer  "author_id"
    t.string   "er_attach"
  end

  add_index "estimate_requisitions", ["author_id"], name: "index_estimate_requisitions_on_author_id"
  add_index "estimate_requisitions", ["job_id"], name: "index_estimate_requisitions_on_job_id"
  add_index "estimate_requisitions", ["vendor_id"], name: "index_estimate_requisitions_on_vendor_id"

  create_table "estimates", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "job_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "serial_number"
    t.string   "estimate_type"
    t.date     "printer_date"
    t.string   "printer_title"
    t.string   "amount"
    t.text     "notes"
    t.string   "signoff"
    t.string   "signatory"
    t.string   "client_signatory"
    t.integer  "billable_entity_id"
    t.integer  "vendor_id"
    t.integer  "author_id"
    t.string   "status"
  end

  add_index "estimates", ["author_id"], name: "index_estimates_on_author_id"
  add_index "estimates", ["billable_entity_id"], name: "index_estimates_on_billable_entity_id"
  add_index "estimates", ["job_id"], name: "index_estimates_on_job_id"
  add_index "estimates", ["vendor_id"], name: "index_estimates_on_vendor_id"

  create_table "holidays", force: :cascade do |t|
    t.string   "name"
    t.date     "whichday"
    t.boolean  "mumbai",     default: false
    t.boolean  "delhi",      default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "ideas", force: :cascade do |t|
    t.string   "idea"
    t.integer  "job_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "ideas", ["job_id"], name: "index_ideas_on_job_id"

  create_table "images", force: :cascade do |t|
    t.string   "image"
    t.integer  "job_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "images", ["job_id"], name: "index_images_on_job_id"

  create_table "invoice_requisitions", force: :cascade do |t|
    t.text     "notes"
    t.integer  "estimate_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "approval"
    t.integer  "author_id"
  end

  add_index "invoice_requisitions", ["author_id"], name: "index_invoice_requisitions_on_author_id"
  add_index "invoice_requisitions", ["estimate_id"], name: "index_invoice_requisitions_on_estimate_id"

  create_table "invoices", force: :cascade do |t|
    t.string   "title"
    t.text     "details"
    t.integer  "estimate_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "serial_number"
    t.string   "invoice_type"
    t.decimal  "amount"
    t.decimal  "service_tax"
    t.decimal  "swachh_bharat"
    t.decimal  "krishi_kalyan_cess"
    t.decimal  "total_amount"
    t.string   "amount_in_words"
    t.text     "notes"
    t.string   "signoff"
    t.string   "signatory"
    t.string   "client_signatory"
    t.string   "paid_status"
    t.date     "printer_date"
    t.string   "printer_title"
    t.integer  "billable_entity_id"
    t.integer  "author_id"
  end

  add_index "invoices", ["author_id"], name: "index_invoices_on_author_id"
  add_index "invoices", ["billable_entity_id"], name: "index_invoices_on_billable_entity_id"
  add_index "invoices", ["estimate_id"], name: "index_invoices_on_estimate_id"

  create_table "job_watchers", id: false, force: :cascade do |t|
    t.integer "job_id",  null: false
    t.integer "user_id", null: false
  end

  add_index "job_watchers", ["job_id", "user_id"], name: "index_job_watchers_on_job_id_and_user_id"
  add_index "job_watchers", ["user_id", "job_id"], name: "index_job_watchers_on_user_id_and_job_id"

  create_table "jobs", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "client_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.date     "brief_date"
    t.date     "deadline"
    t.string   "serial_number"
    t.integer  "author_id"
    t.integer  "status_id"
  end

  add_index "jobs", ["author_id"], name: "index_jobs_on_author_id"
  add_index "jobs", ["client_id"], name: "index_jobs_on_client_id"
  add_index "jobs", ["status_id"], name: "index_jobs_on_status_id"

  create_table "jobs_tags", id: false, force: :cascade do |t|
    t.integer "job_id", null: false
    t.integer "tag_id", null: false
  end

  add_index "jobs_tags", ["job_id", "tag_id"], name: "index_jobs_tags_on_job_id_and_tag_id"
  add_index "jobs_tags", ["tag_id", "job_id"], name: "index_jobs_tags_on_tag_id_and_job_id"

  create_table "leave_applications", force: :cascade do |t|
    t.string   "leave_type"
    t.text     "details"
    t.date     "from_date"
    t.date     "to_date"
    t.string   "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "author_id"
  end

  add_index "leave_applications", ["author_id"], name: "index_leave_applications_on_author_id"

  create_table "purchase_orders", force: :cascade do |t|
    t.string   "title"
    t.integer  "amount"
    t.integer  "estimate_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "serial_number"
    t.string   "amount_in_words"
    t.text     "details"
    t.string   "payment_terms"
    t.string   "bill_given_status"
    t.string   "client_paid_status"
    t.string   "vendor_paid_status"
    t.integer  "vendor_id"
    t.integer  "author_id"
  end

  add_index "purchase_orders", ["author_id"], name: "index_purchase_orders_on_author_id"
  add_index "purchase_orders", ["estimate_id"], name: "index_purchase_orders_on_estimate_id"
  add_index "purchase_orders", ["vendor_id"], name: "index_purchase_orders_on_vendor_id"

  create_table "retainer_invoices", force: :cascade do |t|
    t.string   "title"
    t.text     "narration"
    t.integer  "client_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "serial_number"
    t.decimal  "amount"
    t.decimal  "service_tax"
    t.decimal  "swachh_bharat"
    t.decimal  "krishi_kalyan_cess"
    t.string   "total_amount"
    t.string   "amount_in_words"
    t.text     "notes"
    t.string   "signoff"
    t.string   "signatory"
    t.string   "status"
    t.string   "printer_title"
    t.date     "printer_date"
    t.integer  "billable_entity_id"
    t.integer  "author_id"
  end

  add_index "retainer_invoices", ["author_id"], name: "index_retainer_invoices_on_author_id"
  add_index "retainer_invoices", ["billable_entity_id"], name: "index_retainer_invoices_on_billable_entity_id"
  add_index "retainer_invoices", ["client_id"], name: "index_retainer_invoices_on_client_id"

  create_table "roles", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "role"
    t.integer  "client_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "roles", ["client_id"], name: "index_roles_on_client_id"
  add_index "roles", ["user_id"], name: "index_roles_on_user_id"

  create_table "statuses", force: :cascade do |t|
    t.string "name"
    t.string "color"
  end

  create_table "tags", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "unbillable_purchase_orders", force: :cascade do |t|
    t.text     "details"
    t.integer  "amount"
    t.integer  "client_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "bill_received_status"
    t.string   "bill_paid_status"
    t.integer  "vendor_id"
    t.string   "serial_number"
    t.integer  "author_id"
    t.string   "approval_status"
  end

  add_index "unbillable_purchase_orders", ["author_id"], name: "index_unbillable_purchase_orders_on_author_id"
  add_index "unbillable_purchase_orders", ["client_id"], name: "index_unbillable_purchase_orders_on_client_id"
  add_index "unbillable_purchase_orders", ["vendor_id"], name: "index_unbillable_purchase_orders_on_vendor_id"

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "admin",                  default: false
    t.datetime "archived_at"
    t.boolean  "finance",                default: false
    t.integer  "casual",                 default: 7
    t.integer  "sick",                   default: 10
    t.integer  "privileged",             default: 30
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

  create_table "vendors", force: :cascade do |t|
    t.string   "name"
    t.string   "contact_name"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "ventype_id"
    t.string   "phone"
    t.string   "email"
    t.text     "address"
    t.string   "bank"
    t.string   "bank_acct"
    t.string   "ifsc"
    t.string   "service_tax"
    t.string   "pan"
    t.string   "tan"
  end

  add_index "vendors", ["ventype_id"], name: "index_vendors_on_ventype_id"

  create_table "ventypes", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "wassups", force: :cascade do |t|
    t.string   "title"
    t.text     "text"
    t.string   "wimage"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
