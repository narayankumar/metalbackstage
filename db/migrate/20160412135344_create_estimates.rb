class CreateEstimates < ActiveRecord::Migration
  def change
    create_table :estimates do |t|
      t.string :name
      t.text :description
      t.references :job, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
