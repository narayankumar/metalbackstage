class RenameColumnsInRetainerInvoices < ActiveRecord::Migration
  def change
    rename_column :retainer_invoices, :higher_secondary_cess, :krishi_kalyan_cess
  end
end

