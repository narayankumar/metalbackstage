class CreateWassups < ActiveRecord::Migration
  def change
    create_table :wassups do |t|
      t.string :title
      t.text :text
      t.string :wimage
      
      t.timestamps null: false
    end
  end
end
