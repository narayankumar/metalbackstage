class RemoveEducationFromInvoices < ActiveRecord::Migration
  def change
    remove_column :invoices, :education_cess, :decimal
  end
end
