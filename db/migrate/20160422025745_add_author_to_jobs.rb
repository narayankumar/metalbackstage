class AddAuthorToJobs < ActiveRecord::Migration
  def change
    add_reference :jobs, :author, index: true
    add_foreign_key :jobs, :users, column: :author_id
  end
end
