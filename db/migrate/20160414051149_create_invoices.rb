class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.string :name
      t.text :details
      t.references :estimate, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
