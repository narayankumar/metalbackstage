class CreateLeaveApplications < ActiveRecord::Migration
  def change
    create_table :leave_applications do |t|
      t.string :leave_type
      t.text :details
      t.date :from_date
      t.date :to_date
      t.string :status

      t.timestamps null: false
    end
    add_reference :leave_applications, :author, index: true
    add_foreign_key :leave_applications, :users, column: :author_id
  end
end
