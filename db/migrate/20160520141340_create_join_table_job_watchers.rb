class CreateJoinTableJobWatchers < ActiveRecord::Migration
  def change
    create_join_table :jobs, :users, table_name: :job_watchers do |t|
      t.index [:job_id, :user_id]
      t.index [:user_id, :job_id]
    end
  end
end
