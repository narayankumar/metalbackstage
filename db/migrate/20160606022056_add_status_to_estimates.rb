class AddStatusToEstimates < ActiveRecord::Migration
  def change
    add_column :estimates, :status, :string
  end
end
