class AddAuthorToEstimateRequisitions < ActiveRecord::Migration
  def change
    add_reference :estimate_requisitions, :author, index: true
    add_foreign_key :estimate_requisitions, :users, column: :author_id
  end
end
