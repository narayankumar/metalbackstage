class CreateEstfiles < ActiveRecord::Migration
  def change
    create_table :estfiles do |t|
      t.string :file
      t.references :estimate, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
