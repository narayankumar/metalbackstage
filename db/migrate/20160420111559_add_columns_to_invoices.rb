class AddColumnsToInvoices < ActiveRecord::Migration
  def change
    add_column :invoices, :title, :string
    add_column :invoices, :invoice_type, :string
    add_column :invoices, :amount, :decimal
    add_column :invoices, :service_tax, :decimal
    add_column :invoices, :swachh_bharat, :decimal
    add_column :invoices, :education_cess, :decimal
    add_column :invoices, :higher_secondary_cess, :decimal
    add_column :invoices, :total_amount, :decimal    
    add_column :invoices, :amount_in_words, :string
    
    add_column :invoices, :notes, :text    
    add_column :invoices, :signoff, :string
    add_column :invoices, :signatory, :string
    add_column :invoices, :client_signatory, :string
    add_column :invoices, :paid_status, :string
    add_column :invoices, :printer_date, :date
    add_column :invoices, :printer_title, :string
    
    add_reference :invoices, :billable_entity, index: true
  end
end
