class RemoveColumnInRetainerInvoices < ActiveRecord::Migration
  def change
    remove_column :retainer_invoices, :education_cess, :decimal
  end
end
