class AddColumnsToVendors < ActiveRecord::Migration
  def change
    add_column :vendors, :phone, :string
    add_column :vendors, :email, :string
    add_column :vendors, :address, :text
  end
end
