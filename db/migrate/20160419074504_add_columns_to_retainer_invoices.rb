class AddColumnsToRetainerInvoices < ActiveRecord::Migration
  def change
    add_column :retainer_invoices, :amount, :decimal
    add_column :retainer_invoices, :service_tax, :decimal
    add_column :retainer_invoices, :swachh_bharat, :decimal
    add_column :retainer_invoices, :education_cess, :decimal
    add_column :retainer_invoices, :higher_secondary_cess, :decimal
    add_column :retainer_invoices, :total_amount, :string
    add_column :retainer_invoices, :amount_in_words, :string
    add_column :retainer_invoices, :notes, :text
    
    add_column :retainer_invoices, :signoff, :string
    add_column :retainer_invoices, :signatory, :string
    
    add_column :retainer_invoices, :status, :string
    add_column :retainer_invoices, :printer_title, :string
    add_column :retainer_invoices, :printer_date, :date
    
    add_reference :retainer_invoices, :billable_entity, index: true
  end
end
