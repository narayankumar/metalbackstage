class AddAuthorToInvoiceRequisitions < ActiveRecord::Migration
  def change
    add_reference :invoice_requisitions, :author, index: true
    add_foreign_key :invoice_requisitions, :users, column: :author_id
  end
end
