class CreateInvoiceRequisitions < ActiveRecord::Migration
  def change
    create_table :invoice_requisitions do |t|
      t.text :notes
      t.references :estimate, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
