class AddFinanceToUsers < ActiveRecord::Migration
  def change
    add_column :users, :finance, :boolean, default: false
  end
end
