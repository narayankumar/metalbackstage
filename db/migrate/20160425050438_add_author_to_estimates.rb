class AddAuthorToEstimates < ActiveRecord::Migration
  def change
    add_reference :estimates, :author, index: true
    add_foreign_key :estimates, :users, column: :author_id
  end
end
