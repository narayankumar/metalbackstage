class AddAuthorToInvoices < ActiveRecord::Migration
  def change
    add_reference :invoices, :author, index: true
    add_foreign_key :invoices, :users, column: :author_id
  end
end
