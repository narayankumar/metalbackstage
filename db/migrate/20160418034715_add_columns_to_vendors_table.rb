class AddColumnsToVendorsTable < ActiveRecord::Migration
  def change
    add_column :vendors, :bank, :string
    add_column :vendors, :bank_acct, :string
    add_column :vendors, :ifsc, :string
    add_column :vendors, :service_tax, :string
    add_column :vendors, :pan, :string
    add_column :vendors, :tan, :string
  end
end
