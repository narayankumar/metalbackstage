class RemoveAttachmentFromJobs < ActiveRecord::Migration
  def change
    remove_column :jobs, :attachment, :string
  end
end
