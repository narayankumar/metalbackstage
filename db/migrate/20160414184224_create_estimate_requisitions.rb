class CreateEstimateRequisitions < ActiveRecord::Migration
  def change
    create_table :estimate_requisitions do |t|
      t.text :details
      t.integer :amount
      t.references :job, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
