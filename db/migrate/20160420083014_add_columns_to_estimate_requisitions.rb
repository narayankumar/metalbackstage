class AddColumnsToEstimateRequisitions < ActiveRecord::Migration
  def change
    add_column :estimate_requisitions, :title, :string
    add_reference :estimate_requisitions, :vendor, index: true
  end
end
