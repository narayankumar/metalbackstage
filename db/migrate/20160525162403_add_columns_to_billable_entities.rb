class AddColumnsToBillableEntities < ActiveRecord::Migration
  def change
    add_column :billable_entities, :contact_name, :string
    add_column :billable_entities, :email, :string
    add_column :billable_entities, :address, :text
  end
end
