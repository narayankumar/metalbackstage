class RenameColumnInInvoices < ActiveRecord::Migration
  def change
    rename_column :invoices, :higher_secondary_cess, :krishi_kalyan_cess
  end
end
