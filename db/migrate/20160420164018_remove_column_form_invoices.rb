class RemoveColumnFormInvoices < ActiveRecord::Migration
  def change
    remove_column :invoices, :title
    rename_column :invoices, :name, :title
  end
end
