class AddStatusToUnbillablePurchaseOrders < ActiveRecord::Migration
  def change
    add_column :unbillable_purchase_orders, :approval_status, :string
  end
end
