class AddAuthorToPurchaseOrders < ActiveRecord::Migration
  def change
    add_reference :purchase_orders, :author, index: true
    add_foreign_key :purchase_orders, :users, column: :author_id
  end
end
