class AddColumnsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :casual, :integer, default: 7
    add_column :users, :sick, :integer, default: 10
    add_column :users, :privileged, :integer, default: 30
  end
end
