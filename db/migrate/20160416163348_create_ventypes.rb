class CreateVentypes < ActiveRecord::Migration
  def change
    create_table :ventypes do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
