class AddAuthorToRetainerInvoices < ActiveRecord::Migration
  def change
    add_reference :retainer_invoices, :author, index: true
    add_foreign_key :retainer_invoices, :users, column: :author_id
  end
end
