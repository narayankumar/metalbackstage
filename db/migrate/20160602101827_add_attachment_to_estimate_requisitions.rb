class AddAttachmentToEstimateRequisitions < ActiveRecord::Migration
  def change
    add_column :estimate_requisitions, :er_attach, :string
  end
end
