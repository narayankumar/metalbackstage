class AddIdeaToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :idea, :string
  end
end
