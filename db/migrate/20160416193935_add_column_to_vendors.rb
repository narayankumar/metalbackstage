class AddColumnToVendors < ActiveRecord::Migration
  def change
    add_reference :vendors, :ventype, index: true
  end
end
