class CreateUnbillablePurchaseOrders < ActiveRecord::Migration
  def change
    create_table :unbillable_purchase_orders do |t|
      t.text :details
      t.integer :amount
      t.references :client, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
