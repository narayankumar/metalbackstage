class AddColumnsToPurchaseOrders < ActiveRecord::Migration
  def change
    add_column :purchase_orders, :amount_in_words, :string
    add_column :purchase_orders, :details, :text
    add_column :purchase_orders, :payment_details, :text
    add_column :purchase_orders, :payment_terms, :string
    add_column :purchase_orders, :bill_given_status, :string
    add_column :purchase_orders, :client_paid_status, :string
    add_column :purchase_orders, :vendor_paid_status, :string
    
    add_reference :purchase_orders, :vendor, index: true
  end
end
