class AddAuthorToUnbillablePurchaseOrders < ActiveRecord::Migration
  def change
    add_reference :unbillable_purchase_orders, :author, index: true
    add_foreign_key :unbillable_purchase_orders, :users, column: :author_id
  end
end
