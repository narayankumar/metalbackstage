class RemoveImageFromJobs < ActiveRecord::Migration
  def change
    remove_column :jobs, :image, :string
  end
end
