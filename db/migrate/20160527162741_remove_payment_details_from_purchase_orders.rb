class RemovePaymentDetailsFromPurchaseOrders < ActiveRecord::Migration
  def change
    remove_column :purchase_orders, :payment_details, :text
  end
end
