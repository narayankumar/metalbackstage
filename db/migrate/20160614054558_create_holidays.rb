class CreateHolidays < ActiveRecord::Migration
  def change
    create_table :holidays do |t|
      t.string :name
      t.date :when
      t.boolean :mumbai, default: false
      t.boolean :delhi, default: false

      t.timestamps null: false
    end
  end
end
