class CreatePurchaseOrders < ActiveRecord::Migration
  def change
    create_table :purchase_orders do |t|
      t.string :title
      t.integer :amount
      t.references :estimate, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
