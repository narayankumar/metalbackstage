class CreateBillableEntities < ActiveRecord::Migration
  def change
    create_table :billable_entities do |t|
      t.string :name
      t.string :phone
      t.references :client, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
