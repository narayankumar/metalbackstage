class RenameColumnInHolidays < ActiveRecord::Migration
  def change
    rename_column :holidays, :when, :whichday
  end
end
