class AddColumnToInvoiceRequisitions < ActiveRecord::Migration
  def change
    add_column :invoice_requisitions, :approval, :string
  end
end
