class AddColumnsToEstimates < ActiveRecord::Migration
  def change
    add_column :estimates, :type, :string
    add_column :estimates, :printer_date, :date
    add_column :estimates, :printer_title, :string
    add_column :estimates, :amount, :string
    add_column :estimates, :notes, :text
    add_column :estimates, :signoff, :string
    add_column :estimates, :signatory, :string
    add_column :estimates, :client_signatory, :string
    
    add_reference :estimates, :billable_entity, index: true
    add_reference :estimates, :vendor, index: true
  end
end
