class CreateRetainerInvoices < ActiveRecord::Migration
  def change
    create_table :retainer_invoices do |t|
      t.string :title
      t.text :narration
      t.references :client, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
