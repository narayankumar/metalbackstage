class AddPreviousStatusToComments < ActiveRecord::Migration
  def change
    add_reference :comments, :previous_status, index: true
    add_foreign_key :comments, :statuses, column: :previous_status_id
  end
end
