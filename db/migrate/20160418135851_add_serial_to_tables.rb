class AddSerialToTables < ActiveRecord::Migration
  def change
    add_column :unbillable_purchase_orders, :serial_number, :string
    add_column :purchase_orders, :serial_number, :string
    add_column :estimates, :serial_number, :string
    add_column :invoices, :serial_number, :string
    add_column :retainer_invoices, :serial_number, :string
  end
end
