class AddColumnsToUnbillablePurchaseOrders < ActiveRecord::Migration
  def change
    add_column :unbillable_purchase_orders, :bill_received_status, :string
    add_column :unbillable_purchase_orders, :bill_paid_status, :string
    add_reference :unbillable_purchase_orders, :vendor, index: true
  end
end
