class AddColumnsToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :brief_date, :date
    add_column :jobs, :deadline, :date
  end
end
