class RemoveIdeaFromJobs < ActiveRecord::Migration
  def change
    remove_column :jobs, :idea, :string
  end
end
