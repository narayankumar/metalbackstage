class ChangeColumnNameEstimates < ActiveRecord::Migration
  def change
    rename_column :estimates, :type, :estimate_type
  end
end
